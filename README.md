# SiteUP!

![SiteUP! logo](http://siteup.kaan.software/logo_siteup_small.png)

The need to streamline the development of responsive websites using a content management tool (CMS) that is agnostic to the design and representation of data,
that allows to easily interchange parts of its code and to personalize in technology as in design and functionality according to the necessities of development;
is what led me to create the SiteUP! Suite.

Created in open source and using technologies such as _HTML5_, _JS_, _jQuery_, _CSS3_, _PHP_, _Perl_, _MySql_, but allowing them to replace some of them or integrate new ones,
together with online documentation, documented code and the use of standards throughout the system, becomes a viable tool for programmers and software developers
who require not to be Tied to the restrictions of some application.

This project was created more than 10 ago, with another name. I just used to create internal developments and did not know how to market it in time, I do not see
a reason to save something that can be useful for third parties, that is why I make it available to the community. Actually making some other minor changes (v11)
and maybe later update it to new tools and languages like Python, node, and so on.


More information: [SiteUP! website](http://siteup.kaan.software/?lang=en)
Mayor información: [SiteUP! sitio web](http://siteup.kaan.software/?lang=es)


## First steps 🚀

Donwload the files in your web server under your public directory. You will find basic files and two directories; **public/** contains the interface that will be accesible
by the normal user, while **private/** contains the administrative interface. I suggest have a different domain por the last one.

You will need to create a mysql repository with two users assigned to it. One with just basic privileges (select, delete, insert, update, lock) that will be used for public,
and other one for administrative functions with all privileges.

More information: [Requiriments](http://siteup.kaan.software/?lang=en&page=down_struct)
Mayor información: [Requisitos](http://siteup.kaan.software/?lang=es&page=down_struct)


### Pre-requisites 📋

You will need a web server with:

- Apache
- Mysql > v5.0
- PHP > v5.0
  - curl
  - mysqli
- Perl > v5.0
  - Cwd
  - DBI
  - CGI
  - CGI::Carp
  - Digest::MD5
  - Digest::SHA1
  - MIME::Base64
  - Data::Dumper
  - Time::HiRes
  - GD::SecurityImage::AC
  
More information: [Requiriments](http://siteup.kaan.software/?lang=en&page=down_sw)
Mayor información: [Requisitos](http://siteup.kaan.software/?lang=es&page=down_sw)
 

### Installation 🔧

Once copied the files to your root directory, access it via your web browser and follow the instructions.

If correctly installed, the system will delete the installation files and move the **private/** and **public/** directories to correct position,
redirecting you to the administrative console in order to finish the configuration for webmaster.

More information: [Installation](http://siteup.kaan.software/?lang=en&page=down_inst)
Mayor información: [Instalación](http://siteup.kaan.software/?lang=es&page=down_inst) 
 
 
### To develop some new page, service or module ⌨️

To develop a new resource, it is very simple. I alreay has a template to create it, but in order to keep it compatible, all programming
must follow some rules about naming, structure, functions, css classes, and so on.

More information: [Nomenclature](http://siteup.kaan.software/?lang=en&page=docs_cssorg)
Mayor información: [Nomenclatura](http://siteup.kaan.software/?lang=es&page=docs_cssorg)
    

## How to contribute 🖇️

Of course I plan to continue the development, but due is not my only project, not enough time to focus on it.
I hope soon stablish some conduct rules in order to receive pull request, and be published on [CONTRIBUTING.md](CONTRIBUTING.md).


## Wiki 📖

Please visit [SiteUP!](http://siteup.kaan.software/) in order to get documentation about this project. 


## Versions 📌

Previous versions not uploaded.  If you need it to be available, please let me know it.


## Author ✒️
 
* **Alejandro Ceballos** - [personal website](https://alejandro.ceballos.info/) 


## Licence 📄

This project is available under MIT license. See [LICENSE.md](LICENSE.md) for extra details.


## Want to say "thank you"? 🎁

* You can invite me a cup of coffe in order to continue 📢

and / or make a donation to one of the following organizations that helps childrens with cancer.
* [Mi Gran Esperanza](https://www.migranesperanza.org/)
* [Nariz Roja](https://narizroja.org/)
* [Canica](https://canica.org.mx/)
 
More information: [Donations](http://siteup.kaan.software/?lang=en&page=cnt_donate)
Mayor información: [Donaciones](http://siteup.kaan.software/?lang=es&page=cnt_donate)


