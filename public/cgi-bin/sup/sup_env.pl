#!/usr/bin/perl -w

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_env.php - Build 2009
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#  
    my $bool_DEBUG //= 0;


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#
  
#
# -------------------------------- [ Modules ]

# Load modules
    use Cwd;
    use strict;
   
  
#
# -------------------------------- [ General constants and variables ]
     
# Network data

    # local host
    my $str_FULLDOMAIN = lc($ENV{"HTTP_HOST"});
   
    # remote ip
    my $str_IP = $ENV{"REMOTE_ADDR"};
    
    shout('',"network data: localhost:$str_FULLDOMAIN, remote ip:$str_IP") if ($bool_DEBUG);
   
    
# Directories

    # physical path to user home (~user; usually private servers:/var/www/ , shared servers:/home/user/ ) 
    my $str_ROOT = (getcwd() =~ m!^(/home\w*/\w+/)!) ? $1 : '/var/www/';
   
    # physical hidden temporal path directory (usually private servers:/tmp/ , shared servers:/home/user/tmp/ ) 
    my $str_TMP = $str_ROOT.'tmp/';
   
    # physical htdocs directory (usually private servers:/var/www/html/ , shared servers:/home/user/public_html/ )  
    my $str_HTDOCS = $str_ROOT.'public_html/';
   
    # logical home (usually /)
    my $str_HOME = '/';
   
    # logical cgi scripts (usually wamp servers: scripts/ , lamp servers: cgi-bin/ )
    my $str_CGIDIR = $str_HOME.'cgi-bin/';  
   
    # logical visible local temp directory (usually private servers:/var/www/html/temp/ , shared servers:/home/user/public_html/temp/ )
    my $str_TEMP = $str_HOME.'temp/';
   
    shout('',"directories: root:$str_ROOT, tmp:$str_TMP, htdocs:$str_HTDOCS, home:$str_HOME,  cgi:$str_CGIDIR, temp:$str_TEMP ") if ($bool_DEBUG); 


# SiteUP data

    # db data
    my ($str_LANG, @arr_AVAILABLELANGS, @arr_AVAILABLEACTIONS, %hash_default);
    
    # parameter data 
    my %hash_page = {"ID"=>'', "servedID"=>'', "code"=>'', "name"=>'', "privileged"=>'', "usractions"=>''}; 
    my %hash_service = {"ID"=>'', "servedID"=>'', "code"=>'', "name"=>'', "privileged"=>'', "usractions"=>''};  # servedID = ID service served 
    my %hash_subservice = {"ID"=>'', "servedID"=>'', "code"=>'', "name"=>'', "privileged"=>'', "usractions"=>''};  # servedID = ID subservice served
    my $char_action = ''; 
  

#
# ========================================= [ FUNCTIONS ]
#
  
      
#
# -------------------------------- [ No configurable ]
# 
 
    # Recover directory 
    sub envDir 
        {
        foreach ($_[0]) { 
            /root/ && do { return $str_ROOT; last; };
            /tmp/ && do { return $str_TMP; last; };
            /htdocs/ && do { return $str_HTDOCS; last; };
            /home/ && do { return $str_HOME; last; }; 
            /cgi/ && do { return $str_CGIDIR; last; }; 
            /temp/ && do { return $str_TEMP; last; };
            return './';
            }   
        }
 
              
    # Return generic value: variables > default > param        
    sub envGet 
        {
        foreach ($_[0]) {
            /ip/ && do { return $str_IP; last; }; 
            /fulldomain/ && do { return $str_FULLDOMAIN; last; };
            /lang/ && do { return $str_LANG; last; };
            # security code (generated during installation; must match with sup_env.php)
            /seccode/ && do { return '#SECCODE#'; last; }; 
            return ($hash_default{$_[0]}) ? $hash_default{$_[0]} : '';
            }
        }
         
    # Type of information to load         
    sub envIs    
        {   
        foreach ($_[0]) {
            /page/ && do { return ($hash_page{'ID'}) ? 1 : 0; last; }; 
            /service/ && do { return ($hash_service{'ID'}) ? 1 : 0; last; }; 
            /subservice/ && do { return ($hash_subservice{'ID'}) ? 1 : 0; last; }; 
            }      
        } 
   
    # Page information           
    sub envPage    
        {   
        foreach ($_[0]) {
            /id/ && do { return $hash_page{'ID'}; last; };  
            /code/ && do { return $hash_page{'code'}; last; }; 
            /name/ && do { return $hash_page{'name'}; last; }; 
            /privileged/ && do { return $hash_page{'privileged'}; last; }; 
            /usractions/ && do { return $hash_page{'usractions'}; last; }; 
            } 
        }      
      
    # Service information           
    sub envServ    
        {   
        foreach ($_[0]) {
            /id/ && do { return $hash_service{'ID'}; last; }; 
            /servedid/ && do { return $hash_service{'servedID'}; last; }; 
            /code/ && do { return $hash_service{'code'}; last; }; 
            /name/ && do { return $hash_service{'name'}; last; }; 
            /privileged/ && do { return $hash_service{'privileged'}; last; }; 
            /usractions/ && do { return $hash_service{'usractions'}; last; }; 
            } 
        }            
        
    # Service information           
    sub envSubs    
        {   
        foreach ($_[0]) {
            /id/ && do { return $hash_subservice{'ID'}; last; }; 
            /servedid/ && do { return $hash_subservice{'servedID'}; last; }; 
            /code/ && do { return $hash_subservice{'code'}; last; }; 
            /name/ && do { return $hash_subservice{'name'}; last; }; 
            /privileged/ && do { return $hash_subservice{'privileged'}; last; }; 
            /usractions/ && do { return $hash_subservice{'usractions'}; last; }; 
            } 
        }     
            
    # Return default value 
    sub envDefault 
        { return ($_[0]) ? $hash_default{$_[0]} : %hash_default; }


#
# ========================================= [ MAIN BODY (initialization) ]
#

    # Initialize
   
    # -- SiteUP config data from DB
    
    # init
    dbLogin();
     
    # all defaults
    dbQuery("SELECT * FROM ".dbPrefix()."config "); 
    while (my($str_thisID, $str_thisname) = dbFetch()) 
        { $hash_default{$str_thisID} = $str_thisname; }  
    shout('%',\%hash_default) if ($bool_DEBUG);
      
    # Available languages (detect by files in $str_HOMEDIR/lang/)
    @arr_AVAILABLELANGS = split(//,$hash_default{'LANGS'}); 
    # Available actions (default)
    @arr_AVAILABLEACTIONS = split(//,'abcdefghik');
    # Main language (first of detected langs)
    $str_LANG = $arr_AVAILABLELANGS[0];
   
    # -- SiteUP! service / page 

    # get service info
    if (param('service')) { 
        ($hash_service{"ID"}, $hash_service{"servedID"}) = (ref(param("service")) eq 'ARRAY') ? param("service") : (param("service"), '');
        $hash_service{"ID"} =~ s/\W//g; 
        dbQuery("SELECT id, code, name, privileged, usractions FROM ".dbPrefix()."service WHERE (id = '".$hash_service{"ID"}."') AND (active = '1') LIMIT 1 ");               
        ($hash_service{"ID"}, $hash_service{"code"}, $hash_service{"name"}, $hash_service{"privileged"}, $hash_service{"usractions"}) = dbFetch();
        if ($hash_service{"ID"}) { 
            $hash_service{"servedID"} = param($hash_service{"code"}) unless ($hash_service{"servedID"});
            $hash_service{"servedID"} =~ s/\W//g; 
            shout('%',%hash_service) if ($bool_DEBUG);  
            # get subservice info
            if (param('subservice')) { 
                ($hash_subservice{"ID"}, $hash_subservice{"servedID"}) = (ref(param("subservice")) eq 'ARRAY') ? param("subservice") : (param("subservice"), '');
                $hash_subservice{"ID"} =~ s/\W//g; 
                dbQuery("SELECT id, code, name, privileged, usractions FROM ".dbPrefix()."subservice WHERE (id = '".$hash_subservice{"ID"}."') AND (active = '1') LIMIT 1 ");    #  AND (service_id = '".$hash_service{"ID"}."')            
                ($hash_subservice{"ID"}, $hash_subservice{"code"}, $hash_subservice{"name"}, $hash_subservice{"privileged"}, $hash_subservice{"usractions"}) = dbFetch();
                if ($hash_subservice{"ID"}) {
                    $hash_subservice{"servedID"} = param($hash_subservice{"code"}) unless ($hash_subservice{"servedID"});
                    $hash_subservice{"servedID"} =~ s/\W//g; 
                    shout('%',%hash_subservice) if ($bool_DEBUG);  
                    }  
                }  
            } 
        $char_action = $1 if (param('action') =~ /^(\w)/); 
        }
    elsif (param('page')) {
        $hash_page{"ID"} =~ s/\W//g; 
        dbQuery("SELECT id, code, name, privileged, usractions FROM ".dbPrefix()."page WHERE (id = '".$hash_page{"ID"}."') AND (active = '1') LIMIT 1 ");               
        ($hash_page{"ID"}, $hash_page{"code"}, $hash_page{"name"}, $hash_page{"privileged"}, $hash_page{"usractions"}) = dbFetch();
        if ($hash_page{"ID"}) { 
            $hash_page{"servedID"} = param($hash_page{"code"}) unless ($hash_page{"servedID"});
            $hash_page{"servedID"} =~ s/\W//g; 
            shout('%',%hash_page) if ($bool_DEBUG);
            }
        }
       
   # ends 
    dbLogout();
      
       
1;