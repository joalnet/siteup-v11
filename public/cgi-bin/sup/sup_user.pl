#!/usr/bin/perl -w

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_user.php - Build 2009
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#
    my $bool_DEBUG //= 0;
  
  
#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Modules ]

# Load modules
    use Cwd;
    use MIME::Base64 qw(decode_base64);
    use strict; 


#
# -------------------------------- [ General constants and variables ]
   
    # Authoring 
    my ($str_AUTHOR, $str_SIGNATURE, $bool_SIGNED);        

    # User data
    my %hash_userdata;


#
# ========================================= [ FUNCTIONS ]
#
 
    # Generate current system datetime   
    sub now
        {
        my ($int_year, $int_mon, $int_day, $int_hour, $int_min, $int_sec) = (localtime(time))[5,4,3,2,1,0];
        return sprintf("%4d-%02d-%02d %02d:%02d:%02d",$int_year+1900,$int_mon+1,$int_day,$int_hour,$int_min,$int_sec); 
        }
 
 
    # Verify adm | usr identify | Get ID | author identity
    sub usrIs 
        {
        for ($_[0]) { 
            /privileged/ && do { return $hash_userdata{'privileged'}; last; };
            /signed/ && do { return $bool_SIGNED; last; };
            } 
        }

    # Retrieve User data
      sub usrGet 
        {
        if (usrIs('signed')) {
            for ($_[0]) {
                /id/ && do { return $hash_userdata{'ID'}; last; };
                /name/ && do { return $hash_userdata{'name'}; last; };
                /login/ && do { return $hash_userdata{'login'}; last; }; 
                /lang/ && do { return $hash_userdata{'lang'}; last; };
                /profile/ && do { return $hash_userdata{'profile'}; last; };
                /author/ && do { return 'usr:'.$hash_userdata{'ID'}; last; }; 
                /signature/ && do { return join(':','usr'.$hash_userdata{'ID'},now()); last; }; 
                /drand/ && do { return (my $a = $hash_userdata{'gibberish'}) =~ s/\D|\W//g; last; };  
                /wrand/ && do { return (my $a = $hash_userdata{'gibberish'}) =~ s/\d|\W//g; last; };  
                /rand/ && do { return $hash_userdata{'gibberish'}; last; };
                } 
            } 
        }
 
#
# -------------------------------- [ No configurable ]
# 

# --- Initialize (load cookie data) 
     
    # recover data from cookie 
    if (cookie('user') && cookie('session')) {
         # recover data from cookies
         my ($str_thiscode1,$str_thiscode2);
         ($hash_userdata{'ID'},$hash_userdata{'name'},$hash_userdata{'profile'},$str_thiscode1) = split("\t",decode_base64(cookie('user')));
         ($hash_userdata{'login'},$hash_userdata{'gibberish'},$hash_userdata{'lang'},$str_thiscode2) = split("\t",decode_base64(cookie('session')));
         shout('%',%hash_userdata) if ($bool_DEBUG);
         # verify against db
         dbLogin();
         dbQuery("SELECT id FROM ".dbPrefix()."user WHERE (id = '".$hash_userdata{'ID'}."') AND (active = '1') LIMIT 1 ");
         ($hash_userdata{'ID'}) = dbFetch(); 
         dbLogout();
         # if id and code match
         if ($hash_userdata{'ID'} && ($str_thiscode1 eq $str_thiscode2) && $str_thiscode1) {
             $bool_SIGNED = 1; 
             }
         else {
             $bool_SIGNED = 0;
             %hash_userdata = {};
             } 
         } 
 
     
1;