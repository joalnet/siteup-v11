#!/usr/bin/perl -w

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_db.php - Build 2009
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
# 
    my $bool_DEBUG //= 0;
  

#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Modules ]

# Load modules
    use DBI;
    use strict;
  
     
# Local modules
    #require('../slib/debug.pl'); 

  
#
# -------------------------------- [ General constants and variables ]
  
# DB connection
    my ($db_this, $sth_this);

#
# ========================================= [ FUNCTIONS ]
#
 
#
# -------------------------------- [ Customized ]
#

# DB login

    sub dbLogin
        { 
        # Host name 
        my $str_DBHOST = '#DBHOST#';
        # Db user 
        my $str_DBUSER = '#DBUSRLOG#';
        # Db password
        my $str_DBPWD = '#DBUSRPWD#';
        # Db name
        my $str_DBNAME = '#DBNAME#';
        #
        shout('i',"login: ".obscure($str_DBUSER)." on ".obscure($str_DBNAME)) if ($bool_DEBUG);  
        if ($db_this = DBI->connect("DBI:mysql:$str_DBNAME:$str_DBHOST",$str_DBUSER,$str_DBPWD)) {
            $sth_this = $db_this->do("SET names 'utf8'");
            $sth_this = $db_this->do("SET character set utf8");
            $sth_this = $db_this->do("SET character_set_client = 'utf8'"); 
            $sth_this = $db_this->do("SET character_set_connection = 'utf8'");   
            $sth_this = $db_this->do("SET character_set_db_results = 'utf8'");
            $sth_this = $db_this->do("SET collation_connection = 'utf8_general_ci'");
            }
        else
            { dbError(); }
        }
   
   
# Table prefix

    sub dbPrefix 
        {
        my $str_TABLEPREFIX = '#TABLEPREFIX#';
        return $str_TABLEPREFIX;
        }


#
# -------------------------------- [ No configurable ]
# 
 
# DB error msgs

    sub dbError 
        { shout('e',DBI::err." : ".DBI::errstr); } 


# DB type

    sub dbType 
        { return 'MySQL '.DBI->installed_versions; }
    
    
# DB query

    sub dbQuery
        {
        my $str_query = $_[0];
        #
        $str_query =~ s/^\s+|\s+$//g;
        shout('q',$str_query) if ($bool_DEBUG);  
        if ($str_query =~ /^SELECT/i) {
            $sth_this = $db_this->prepare($str_query) or dbError();
            $sth_this->execute() or dbError();
            }
        else
            { $sth_this = $db_this->do($str_query) or dbError(); }
        }

        
# DB condition

    sub dbCondition
        {
        my @arr_this = $_[0];
        # 
        my $str_connect = ($arr_this[0] eq '&&') ? 'AND' : 'OR';
        if (ref($arr_this[1]) eq 'ARRAY') {
            # connect, array, array
            return dbCondition($arr_this[1]).$str_connect.dbCondition($arr_this[2]);
            }
        elsif (ref($arr_this[1]) eq 'SCALAR') {
            my $str_dbfield = $arr_this[1];
            my $str_operator = '=';
            my @arr_elements;
            if ($arr_this[2] =~ /^[\W\S]+$/) {
                # connect, dbfield, operator, array
                $str_operator = $arr_this[2];
                shift @arr_this;
                }
            # else connect, dbfield, array 
            foreach (@{$arr_this[2]})
                { push (@arr_elements, "($str_dbfield $str_operator '$_')"); }
            return join(" $str_connect ",@arr_elements); 
            }
        else { return "('error' = 'on dbCondition')"; }    
        }
        
        
# DB clear quotes

    sub dbClrQuotes
        {
        foreach (@_)
            { s/(\\|\')/\$1/g; }
        return (wantarray) ? @_ : $_[0];
        }
          
      
# Recover array | hash with results

    sub dbFetch 
        { return ($_[0] ne 'hash') ? $sth_this->fetchrow_array() : %{$sth_this->fetchrow_hashref()}; } # &{}? -> validate
        
        
# Recover rows | array of arrays with all result (use carefully!) | free results

    sub dbResult 
        {
        my $k = $_[0];
        #
        my $i = $sth_this->rows();
        shout('',"$i results found") if ($bool_DEBUG);
        if ($k eq 'rows') 
            { return $i; }
        elsif ($k eq 'free')
            { $sth_this->finish(); }
        elsif ($i<500) {
            my (@arr_this, @aa_result);
            while (@arr_this = dbFetch('row'))
              { push(@aa_result, @arr_this); }
            return ($i) ? @aa_result : 0;  
            }
        else { return -1; }
        }


# DB logout

    sub dbLogout
        {
        shout('i',"logout") if ($bool_DEBUG); 
        $db_this->disconnect() or dbError();
        }
    
  
1;