/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  validate.js - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */
 
  
// -------------- Data Type
 
   // string?
   function isString(t)
      { return (typeof t === 'string' || t instanceof String); }
 
   // array?
   function isArray(t)
      { return (typeof t === 'array' || t instanceof Array); }
 
  
// -------------- SiteUP!

   // alhpanum >= 12 
   function isId(x)
      {
      if (isString(x))  { 
         x = x.replace(/\W/g,'');
	return (x.length >= 12) ? x : false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isId(x[i]); }
	return x;
	} 
      return false;
      }
      
   // is w substring of x (returns array reduced by -1)
   function isSubstr(w,x)
      {
      var r;
      if (isString(x))
         { r = (/w/.test(x)) ? x : 0; }
      else if (isArray(x))
         { for (var i in x) { r[i] = (/w/.test(x[i]) ? x[i] : false); } } 
      return r; 
      }

   // is all values not false
   function isAlltrue(x)
      {
      if (isString(x))
         { return (x != false) ? 1 : 0; }
      else if (isArray(x)) {
	var bool_true = true;
         for (var i in x) { bool_true &= (x[i] != false); }
	return (bool_true) ? 1 : 0;
	} 
      return false; 
      }

 
// -------------- Boolean

   // boolean as 1 or 0
   function isBool(x)
      {
      if (isString(x))
         { return (x == 0) ? 0 : 1; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isBool(x[i]); }
	return x;
	} 
      return false;
      }  
 
// -------------- String

   // 1 char
   function isChar(k)
      {
      if (isString(x))  {
         x = /\w/.exec(x); 
	return (x.length == 1) ? x : false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isChar(x[i]); }
	return x;
	} 
      return false;
      }

   // no space
   function isWord(x)
      {
      if (isString(x))
         { return x.replace(/\s/g,''); }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isWord(x[i]); }
	return x;
	} 
      return false;
      }

   // no space at border or consecutive
   function isCollapsed(x)
      {
      if (isString(x)) 
         { return x.replace(/\s+/g,' ').trim(); }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isCollapsed(x[i]); }
	return x;
	} 
      return false;
      }
 
// -------------- Numeric

   // Integer 
   function isInt(x)
      {
      if (isString(x)) {
	if (!Number.isInteger(x)) return false;
         x = x.replace(/.*(\+|\-)?\s*(\d+).*/,"$1 $2"); 
	return (parseInt(x) != 0) ? x : 0;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isInt(x[i]); }
	return x;
	} 
      return false;
      }
 
   // Real, not scientific notation
   function isReal(x)
      {
      if (isString(x)) { 
	if (isNaN(x)) return false;
         x = x.replace(/.*(\+|\-)?\s*(\d+)(\.\d*).*/,"$1 $2$3"); 
	return (parseFloat(x) != 0) ? x : 0;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isReal(x[i]); }
	return x;
	} 
      return false;
      }
 
// -------------- Date and time
  
   // Date yyyy-mm-dd
   function isDate(x)
      {
      if (isString(x)) {
	if ((/\d+(?:\-|\/)?(\d+)(?:\-|\/)?\d+/.test(x)) && (1<=parseInt($1)) && (parseInt($1)<=12)) {
	   if (/(\d{4})(?:\-|\/)?(\d{1,2})(?:\-|\/)?(\d{1,2})/.test(x))
	      { return "$1-$2-$3"; }
	   else if (/(\d{1,2})(?:\-|\/)?(\d{1,2})(?:\-|\/)?(\d{4})/.test(x))
	      { return "$3-$2-$1"; } 
	   }
	return false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isDate(x[i]); }
	return x;
	} 
      return false;
      }

   // Date dd-mm-yyyy
   function isDatedmy(x)
      {
      if (isString(x)) {
	if ((/\d+(?:\-|\/)?(\d+)(?:\-|\/)?\d+/.test(x)) && (1<=parseInt($1)) && (parseInt($1)<=12)) {
	   if (/(\d{1,2})(?:\-|\/)?(\d{1,2})(?:\-|\/)?(\d{4})/.test(x))
	      { return "$1-$2-$3"; }
            else if (/(\d{4})(?:\-|\/)?(\d{1,2})(?:\-|\/)?(\d{1,2})/.test(x))
	      { return "$3-$2-$1"; }
	   } 
	return false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isDate(x[i]); }
	return x;
	} 
      return false;
      }

   // Time hh:mm:ss  
   function isTime(x)
      {
      if (isString(x))
         { return ((/(\d+):(\d+):(\d+)/.test(x)) && (parseInt($1)<=23) && (parseInt($2)<=60) && (parseInt($3)<=60)) ? "$1:$2:$3" : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isTime(x[i]); }
	return x;
	} 
      return false;
      }
      
   // Datetime   yyyy-mm-dd hh:mm:ss
   function isDtime(x)
      {
      if (isString(x)) { 
	var a = split(/\s+/,x);
	return (isDate(a[0]) && isTime(a[1])) ? join(' ',$1,$2) : false; 
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isDtime(x[i]); }
	return x;
	} 
      return false;
      }
 
// -------------- Internet

   // avoid html|sql dangerous tags
   function isText(x)
      {
      if (isString(x))
         { return x.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/(\'|\\|\")/g,"\\$1"); }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isText(x[i]); }
	return x;
	} 
      return false;
      }   
  
   // Domain www.example.com
   function isDomain(x)
      {
      if (isString(x))
         { return (/(([\w-]+\.)+\w{2,})/.test(x)) ? $1.toLowerCase() : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isDomain(x[i]); }
	return x;
	} 
      return false;
      }   

   // Website http://www.example.com/
   function isWebsite(x)
      {
      if (isString(x))
	{ return (/(([\w-]+\.)+\w{2,})/.test(x)) ? 'http://'+$1.toLowerCase()+'/' : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isWebsite(x[i]); }
	return x;
	} 
      return false;
      }

   // FTP site ftp://www.example.com/
   function isFtpsite(x)
      {
      if (isString(x))
	{ return (/(([\w-]+\.)+\w{2,})/.test(x)) ? 'ftp://'+$1.toLowerCase()+'/' : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isWebsite(x[i]); }
	return x;
	} 
      return false;
      }

   // Url http://www.example.com/pathto/resource.file
   function isUrl(x)
      {
      if (isString(x))
	{ return (/^((?:http|ftp):\/\/)(([\w-]+\.)+\w{2,})(\/?.*)/i.test(x)) ? $1+$2.toLowerCase()+$4 : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isUrl(x[i]); }
	return x;
	} 
      return false;
      }
      
   // Email something@example.com  
   function isEmail(x)
      {
      if (isString(x)) { 
         var a = split("@",x);
	return (isWord(a[0]) && isDomain(a[1])) ? "$1@$2" : false; 
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isEmail(x[i]); }
	return x;
	} 
      return false;
      }  
  
   // Hex Color	#012345 | #012
   function isHexcolor(x)
      {
      if (isString(x))
         { return (/#?(([0-9a-fA-F]{3}){1,2})/.test(x)) ? '#'+$1.toLowerCase(): false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isHexcolor(x[i]); }
	return x;
	} 
      return false;
      }
  
   // Ip nnn.nnn.nnn.nnn  | nnnn:nnnn:nnnn:nnnn:nnnn:nnnn:nnnn:nnnn ( :: not supported )
   function isIp(x)
      {
      if (isString(x))
         { return (/((\d{1,3}(\.\d{1,3}){3})|(\d{4}(:\d{4}){7}))/.test(x)) ? $1 : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isIp(x[i]); }
	return x;
	} 
      return false;
      }      
 
// -------------- Social

   // Phone (nnn) nnn-nnnn 
   function isPhone(x)
      {
      if (isString(x)) { 
         x.replace(/\D/g,'');
	if ((13 >= x.length) && (x.length >= 7)) {
	   /(\d+)?(\d{3})(\d{4})/.test(x);
	   return (x.length > 7) ? "($1) $2-$3" : "$2-$3";
	   }
	return false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isPhone(x[i]); }
	return x;
	} 
      return false;
      }

   // Rfc ABCDnnnnnnWWW | ABCnnnnnnWWW
   function isRfc(x)
      {
      if (isString(x)) { 
         x.replace(/\W|_/,'').toUpperCase();
	return (/[A-Z]{3,4}\d{6}\w{3}/.test(x)) ? x : false;
	}
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isRfc(x[i]); }
	return x;
	} 
      return false;
      }

   // Social security 
   function isSocialsecurity(x)
      {
      if (isString(x))
         { return (/(?!000|666)[0-8][0-9]{2}-(?!00)[0-9]{2}-(?!0000)[0-9]{4}/.test(x)) ? x : false; }
      else if (isArray(x)) { 
         for (var i in x) { x[i] = isSocialsecurity(x[i]); }
	return x;
	} 
      return false;
      }
 
