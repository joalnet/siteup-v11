/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  siteup.js - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */


// -------------- Modify prototypes

// Capitalize an string
   String.prototype.capitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1);
      }


// -------------- Global variables 
// -------------- Wait to load document

   $(document).ready(function() {
  
      // Label change 
      $('.label').each(function(){ 
         if ($(this).is('input'))
            { if ($(this).val() in hash_DICT) { $(this).val(hash_DICT[$(this).val()]); } }
         else if ($(this).is('a'))
            { if ($(this).attr('alt') in hash_DICT) { $(this).attr('alt',hash_DICT[$(this).attr('alt')]); } }
         else if ($(this).is('img'))
            { if ($(this).attr('title') in hash_DICT) { $(this).attr('title',hash_DICT[$(this).attr('title')]); } }
         else if ($(this).is('input'))
            { if ($(this).attr('placeholder') in hash_DICT) { $(this).attr('placeholder',hash_DICT[$(this).attr('placeholder')]); } }
         else if ($(this).is('optgroup'))
            { if ($(this).attr('label') in hash_DICT) { $(this).attr('label',hash_DICT[$(this).attr('label')]); } }
         else
            { if ($(this).text() in hash_DICT) { $(this).text(hash_DICT[$(this).text()]); } }
         });
      $('label').each(function(){
         if ($(this).text() in hash_DICT) { $(this).text(hash_DICT[$(this).text()]); }
         });
      
      // Form validation
      $('input.validate').blur(function(){
         var str_isfunc = 'is'+$(this).attr('data-is').capitalize();
         var x = eval(str_isfunc+'('+$(this).val()+')');
         if (x) { $(this).val(x); }
         else { $(this).addClass('error'); } 
         });
      $('.error').focus(function(){
         $(this).removeClass('error');
         });
      $('form').submit(function(){
         return ($(this).find(':input').hasClass('error')) ? false : true; 
         });
       

  });