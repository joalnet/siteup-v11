/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  lang_EN.js - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */


   hash_DICT.SERVICE = 'service';
   hash_DICT.SERVICES = 'services';
   hash_DICT.SUBSERVICE = 'subservice';
   hash_DICT.SUBSERVICES = 'subservices';
   hash_DICT.ADMIN = 'manager';
   hash_DICT.ADMINS = 'managers';
   hash_DICT.USER = 'user';
   hash_DICT.USERS = 'users'; 
   hash_DICT.CONFIG = 'configuration';
   hash_DICT.CONFIGS = 'configurations';  
 
   hash_DICT["-1"] = '---';
   hash_DICT["0"] = 'no';
   hash_DICT["1"] = 'yes';
    
   hash_DICT.LOGIN = 'login';
   hash_DICT.PASSWORD = 'password';
   hash_DICT.EMAIL = 'email';
   hash_DICT.SECURITYCODE = 'security code';
   hash_DICT.NOTLOGGED = 'not logged';
   hash_DICT.LOGOUT = 'logout';
   hash_DICT.SEND = 'send';
   hash_DICT.VALIDATE = 'validate';
   
   hash_DICT.INSTALL = 'installation';
   hash_DICT.DEVELOPER = 'installed by';
   hash_DICT.LANG = 'language';
   hash_DICT.CC = 'country';
   hash_DICT.CUR = 'currency';

   hash_DICT.ACTIONS = 'actions';
   hash_DICT.ADM_ACTIONS = 'administrative actions';
   hash_DICT.USR_ACTIONS = 'user actions';
   hash_DICT.ACTION_A = 'add';
   hash_DICT.ACTION_B = 'delete';
   hash_DICT.ACTION_BMODE = 'delete mode';
   hash_DICT.ACTION_BSECURE = 'secure';
   hash_DICT.ACTION_BPURGE = 'purge (not suggested)';
   hash_DICT.ACTION_BRECURSIVE = 'recursive';
   hash_DICT.ACTION_C = 'change';
   hash_DICT.ACTION_D = 'display';
   hash_DICT.ACTION_E = 'export';
   hash_DICT.ACTION_F = 'list';
   hash_DICT.ACTION_FORDER = 'order';
   hash_DICT.ACTION_FASC = 'ascending';
   hash_DICT.ACTION_FDESC = 'descending';
   hash_DICT.ACTION_FORDERBY = 'order by';
   hash_DICT.ACTION_FPAGE = 'page';
   hash_DICT.ACTION_FFIRST = 'first';
   hash_DICT.ACTION_FRWD = 'rewind';
   hash_DICT.ACTION_FPREV = 'previous';
   hash_DICT.ACTION_FACTUAL = 'actual';
   hash_DICT.ACTION_FNEXT = 'next';
   hash_DICT.ACTION_FFWD = 'forward';
   hash_DICT.ACTION_FLAST = 'last';
   hash_DICT.ACTION_G = 'generate';
   hash_DICT.ACTION_H = 'modify system';
   hash_DICT.ACTION_I = 'import';
   hash_DICT.ACTION_K = 'statistics'; 

   hash_DICT.ID = 'id';
   hash_DICT.PID = 'parent id';
   hash_DICT.IID = 'internal id';
   hash_DICT.CREATED = 'created';
   hash_DICT.AUTHOR = 'author'; 
   
   hash_DICT.CATEGORY = 'category';
   hash_DICT.PRIVILEGED = 'privileged';
   hash_DICT.SETUP = 'setup date';
   hash_DICT.NAME = 'name';
   hash_DICT.DESCRIPTION = 'description';
   hash_DICT.ACTIVE = 'active';
   hash_DICT.INACTIVE = 'inactive';
   hash_DICT.RECORDS = 'records';
   hash_DICT.NORECORDS = 'no records';
   hash_DICT.TOTAL = 'total';

   hash_DICT.FMT_DATE = 'yyyy-mm-dd';
   hash_DICT.FMT_TIME = 'hh:mm:ss';
   hash_DICT.FMT_DTIME = 'yyyy-mm-dd hh:mm:ss';
   hash_DICT.FMT_EMAIL = 'user@example.com';
   hash_DICT.FMT_DOMAIN = 'example.com';
   hash_DICT.FMT_FULLDOMAIN = 'www.example.com';
   hash_DICT.FMT_WEBSITE = 'http://www.example.com/';
   hash_DICT.FMT_URL = 'http://www.example.com/resource.some';
   hash_DICT.FMT_PHONE = '98 7654-3210';
   hash_DICT.FMT_PWD = 'uppercase + lowercase + symbol + number > 8';


  