/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  lang_ES.js - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */

   
   hash_DICT.SERVICE = 'servicio';
   hash_DICT.SERVICES = 'servicios';
   hash_DICT.SUBSERVICE = 'subservicio';
   hash_DICT.SUBSERVICES = 'subservicios';
   hash_DICT.ADMIN = 'administrador';
   hash_DICT.ADMINS = 'administradores';
   hash_DICT.USER = 'usuario';
   hash_DICT.USERS = 'usuarios'; 
   hash_DICT.CONFIG = 'configuracion';
   hash_DICT.CONFIGS = 'configuraciones';  

   hash_DICT["-1"] = '---';
   hash_DICT["0"] = 'no';
   hash_DICT["1"] = 'si'; 
    
   hash_DICT.LOGIN = 'clave de acceso';
   hash_DICT.PASSWORD = 'contraseña';
   hash_DICT.EMAIL = 'email';
   hash_DICT.SECURITYCODE = 'código de seguridad';
   hash_DICT.NOTLOGGED = 'no firmado';
   hash_DICT.LOGOUT = 'salir';
   hash_DICT.SEND = 'enviar';
   hash_DICT.VALIDATE = 'validar';
   
   hash_DICT.INSTALL = 'instalación';
   hash_DICT.DEVELOPER = 'instalado por';
   hash_DICT.LANG = 'idioma';
   hash_DICT.CC = 'pais';
   hash_DICT.CUR = 'moneda';

   hash_DICT.ACTIONS = 'acciones'; 
   hash_DICT.ADM_ACTIONS = 'acciones administrativas';
   hash_DICT.USR_ACTIONS = 'acciones de usuario';
   hash_DICT.ACTION_A = 'agregar';
   hash_DICT.ACTION_B = 'borrar';
   hash_DICT.ACTION_BMODE = 'modo de borrado';
   hash_DICT.ACTION_BSECURE = 'seguro';
   hash_DICT.ACTION_BPURGE = 'purgar (no recomendado)';
   hash_DICT.ACTION_BRECURSIVE = 'recursivo';
   hash_DICT.ACTION_C = 'cambiar';
   hash_DICT.ACTION_D = 'desplegar';
   hash_DICT.ACTION_E = 'exportar';
   hash_DICT.ACTION_F = 'listar';
   hash_DICT.ACTION_FORDER = 'orden';
   hash_DICT.ACTION_FASC = 'ascendente';
   hash_DICT.ACTION_FDESC = 'descendente';
   hash_DICT.ACTION_FORDERBY = 'ordenar por';
   hash_DICT.ACTION_FPAGE = 'página';
   hash_DICT.ACTION_FFIRST = 'primer';
   hash_DICT.ACTION_FRWD = 'retroceder';
   hash_DICT.ACTION_FPREV = 'previo';
   hash_DICT.ACTION_FACTUAL = 'actual';
   hash_DICT.ACTION_FNEXT = 'siguiente';
   hash_DICT.ACTION_FFWD = 'avanzar';
   hash_DICT.ACTION_FLAST = 'último';
   hash_DICT.ACTION_G = 'generar';
   hash_DICT.ACTION_H = 'modificar sistema';
   hash_DICT.ACTION_I = 'importar'; 
   hash_DICT.ACTION_K = 'estadísticas'; 

   hash_DICT.ID = 'id';
   hash_DICT.PID = 'id padre';
   hash_DICT.IID = 'id interno';
   hash_DICT.CREATED = 'creado';
   hash_DICT.AUTHOR = 'autor';
   
   hash_DICT.CATEGORY = 'categoria';
   hash_DICT.PRIVILEGED = 'privilegiado';
   hash_DICT.SETUP = 'fecha de creación';
   hash_DICT.NAME = 'nombre';
   hash_DICT.DESCRIPTION = 'descripción';
   hash_DICT.ACTIVE = 'activo';
   hash_DICT.INACTIVE = 'inactivo';
   hash_DICT.RECORDS = 'registros';
   hash_DICT.NORECORDS = 'sin registros';
   hash_DICT.TOTAL = 'total';

   hash_DICT.FMT_DATE = 'yyyy-mm-dd';
   hash_DICT.FMT_TIME = 'hh:mm:ss';
   hash_DICT.FMT_DTIME = 'yyyy-mm-dd hh:mm:ss';
   hash_DICT.FMT_EMAIL = 'user@example.com';
   hash_DICT.FMT_DOMAIN = 'example.com';
   hash_DICT.FMT_FULLDOMAIN = 'wwww.example.com';
   hash_DICT.FMT_WEBSITE = 'http://www.example.com/';
   hash_DICT.FMT_URL = 'http://www.example.com/resource.some';
   hash_DICT.FMT_PHONE = '98 7654-3210';
   hash_DICT.FMT_PWD = 'mayúscula + minúscula + símbolo + número > 8';


