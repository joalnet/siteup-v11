<?php

# ============================================================
#    SiteUP!:   (c)  J. Alejandro Ceballos Z.
#       Name:   sup_user.php - Build 2009
#    License:   MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
    if (preg_match("/sup_user/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
    if ($bool_DEBUG) { shout('p','sup_user'); }
 
 
#
# ========================================= [ IMPLEMENTATION ]
#


    class UserData {
                     
        # --- Domain / subdomain Properties
      
        # Authoring
        private $str_AUTHOR, $str_SIGNATURE, $bool_SIGNED;  
       
        # User data  
        private $hash_userdata;        
           
           
        # --- Methods   
            
        # Initialize 
        public function __construct()
            {
            global $bool_DEBUG, $db;
            # recover data from cookie 
            if (isset($_COOKIE["user"]) && isset($_COOKIE["session"])) {
                 # recover data from cookies
                 $str_thiscode1 = $str_thiscode2 = '';
                 list($hash_userdata['ID'],$hash_userdata['name'],$hash_userdata['profile'],$str_thiscode1) = explode("\t",base64_decode($_COOKIE['user']));
                 list($hash_userdata['login'],$hash_userdata['gibberish'],$hash_userdata['lang'],$str_thiscode2) = explode("\t",base64_decode($_COOKIE['session'])); 
                 if ($bool_DEBUG) shout('%',$hash_userdata);
                 # verify against db
                 $db->dbLogin();
                 $db->dbQuery("SELECT id FROM ".$db->dbPrefix()."user WHERE (id = '".$hash_userdata['ID']."') AND (active = '1') LIMIT 1 ");
                 list ($hash_userdata['ID']) = $db->dbFetch(); 
                 $db->dbLogout();
                 # if id and code match
                 if ($hash_userdata['ID'] && ($str_thiscode1 == $str_thiscode2) && $str_thiscode1) {
                     $bool_SIGNED = 1; 
                     }
                 else {
                     $bool_SIGNED = 0;
                     empty($hash_userdata);
                     } 
                 }  
            }
 
        # Generate current system datetime   
        private function now()
            { return date("Y-m-d H:i:s"); }
 
      
        # Verify adm | usr identify | Get ID | author identity
        public function usrIs($k)
            {
            switch ($k) { 
                 case 'privileged': return $this->hash_userdata['privileged'];  
                 case 'signed': return $this->bool_SIGNED; 
                 } 
             }
     
        # Retrieve User data
        public function usrGet($k) 
             {
             if ($this->usrIs('signed')) {
                 switch ($k) {
                     case 'id': return $this->hash_userdata['ID'];  
                     case 'name': return $this->hash_userdata['name'];  
                     case 'login': return $this->hash_userdata['login'];  
                     case 'lang': return $this->hash_userdata['lang'];  
                     case 'profile': return $this->hash_userdata['profile'];  
                     case 'author': return 'usr:'.$this->hash_userdata['ID'];  
                     case 'signature': return join(':','usr'.$this->hash_userdata['ID'],$this->now()); 
                     case 'drand': return preg_replace('/\D|\W/','',$this->hash_userdata['gibberish']); 
                     case 'wrand': return preg_replace('/\d|\W/','',$this->hash_userdata['gibberish']); 
                     case 'rand': return $this->hash_userdata['gibberish'];  
                     } 
                 } 
             } 

        }


?>