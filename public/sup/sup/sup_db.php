<?php

# ============================================================
#    SiteUP!:   (c)  J. Alejandro Ceballos Z.
#         Name:   sup_db.php - Build 2009
#    License:   MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
    if (preg_match("/sup_db/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
    if ($bool_DEBUG) { shout('p','sup_db'); }


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

    # Previous load by index
    # require_once("debug.php");
 

#
# ========================================= [ IMPLEMENTATION ]
#

    class DbConnection {
      
        # --- Properties
      
        # Host name 
        private $str_DBHOST = '#DBHOST#';
        # Db user 
        private $str_DBUSER = '#DBUSRLOG#';
        # Db password
        private $str_DBPWD = '#DBUSRPWD#';
        # Db name
        private $str_DBNAME = '#DBNAME#';
        # Tables prefix
        private $str_TABLEPREFIX = '#TABLEPREFIX#';
        # DB connection
        private $db_this, $db_result;

        
        # --- Methods 
 
        # DB error msgs
        
        public function dbError()
            { shout('e',$this->db_this->mysqli_connect_errno().": ".$this->db_this->mysqli_connect_error); } 
       
        # DB type
        
        public function dbType()
            { return 'MySQLi '.$this->db_this->client_version; }

        # DB login
        
        public function dbLogin()
            {
            global $bool_DEBUG;
            #
            if ($bool_DEBUG) { shout('i',"login: ".obscure($this->str_DBUSER)." on ".obscure($this->str_DBNAME)); }
            $this->db_this = new mysqli($this->str_DBHOST, $this->str_DBUSER, $this->str_DBPWD, $this->str_DBNAME);
            if (!$this->db_this->connect_errno) {
                $this->db_this->query("SET names = 'utf8'"); 
                $this->db_this->query("SET character set utf8"); 
                $this->db_this->query("SET character_set_client = 'utf8'"); 
                $this->db_this->query("SET character_set_connection = 'utf8'");    
                $this->db_this->query("SET character_set_db_results = 'utf8'");    
                $this->db_this->query("SET collation_connection = 'utf8_general_ci'");
               }
            else { 
                shout('e', $this->db_this->connect_error());  
                $this->db_this->close();
                }
            }

        # DB query
      
        public function dbQuery($str_query)
            {   
            global $bool_DEBUG;
            #
            $str_query = trim($str_query);
            if ($bool_DEBUG) { shout('q',$str_query); } 
            if (preg_match("/^SELECT/",$str_query))
                { $this->db_this->prepare($str_query); }
            $this->db_result = $this->db_this->query($str_query);
            } 
        
        # DB condition

        public function dbCondition($arr_this)
            {
            global $bool_DEBUG;
            # 
            $str_connect = ($arr_this[0] == '&&') ? 'AND' : 'OR';
            if (is_array($arr_this[1])) {
                # connect, array, array
                return $this->dbCondition($arr_this[1]).$str_connect.$this->dbCondition($arr_this[2]);
                }
            elseif (is_scalar($arr_this[1])) {
                $str_dbfield = $arr_this[1];
                $str_operator = '=';
                $arr_elements = array();
                if (preg_match('/^[\W\S]+$/',$arr_this[2])) {
                    # connect, dbfield, operator, array
                    $str_operator = $arr_this[2];
                    array_shift($arr_this);
                    }
                # else connect, dbfield, array
                $arr_elements = array();
                foreach ($arr_this[2] as $str_itemvalue)
                    { push ($arr_elements, "($str_dbfield $str_operator '$str_itemvalue')"); }
                return join(" $str_connect ",$arr_elements); 
                }
            else { return "('error' = 'on dbCondition')"; }    
            } 
        
        # DB clear quotes
      
        public function dbClrQuotes($arr_data)
            {
            for ($i=0; $i<count($arr_data);$i++)    
                { $arr_data[$i] = preg_replace("/(\'|\\)/","\\1",$arr_data[$i]); }
            return $arr_data;    
            } 
         
        # Recover array | hash with results
        
        public function dbFetch($k='arr')
            { return ($k != 'hash') ? $this->db_result->fetch_row() : $this->db_result->fetch_array(MYSQLI_ASSOC); }
             
        # Recover rows | array of arrays with all result (use carefully!) | free results
         
        public function dbResult($k='all')
            {
            global $bool_DEBUG;
            #
            $i = $this->db_result->num_rows;
            if ($bool_DEBUG) { shout('',"$i results found"); }
            if ($k == 'rows')
                { return $i; }
            elseif ($k == 'free')
                { $this->db_result->close(); }
            elseif ($i<500) {
                while ($arr_this = $this->dbFetch('row'))
                    { $aa_result[] = $arr_this; }
                return ($i) ? $aa_result : 0;   
                }
            else { return -1; }
            }
         
        # Logout
      
        public function dbLogout()
            { 
            global $bool_DEBUG;
            #
            if ($bool_DEBUG) { shout('i',"logout"); }
            return mysqli_close($this->db_this) or $this->dbError('logout'); 
            }
       
        # Table prefix
      
        public function dbPrefix()
            { return $this->str_TABLEPREFIX; }
            
        }
    

?>
