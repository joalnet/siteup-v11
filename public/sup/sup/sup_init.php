<?php

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_init.php - Build 1506
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
   if (preg_match("/sup_init/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) { shout('p','sup_init'); }
   
   
#
# ========================================= [ IMPLEMENTATION ]
#

   if ($bool_DEBUG) shout('f',"sup_db");
   require_once("sup/sup/sup_db.php");
   $db = new DbConnection();
  
   if ($bool_DEBUG) shout('f',"sup_user");
   require_once("sup/sup/sup_user.php");
   $usr = new UserData();
  
   if ($bool_DEBUG) shout('f',"sup_env");
   require_once("sup/sup/sup_env.php");
   $env = new EnvData();
  
   if ($usr->usrIs('signed')) { # && isPanel
      if ($bool_DEBUG) shout('f',"sup_nav"); 
      require_once("sup/sup/sup_nav.php");
      $nav = new NavObject();
      }
      
   if ($env->envGet('action')) {
      if ($bool_DEBUG) shout('f',"sup_brw");
      require_once("sup/sup/sup_brw.php");
      $brw = new BrwActions(); 
      }
     
?>
