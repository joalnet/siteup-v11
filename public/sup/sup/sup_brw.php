<?php

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_brw.php - Build 2009
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
    if (preg_match("/sup_brw/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
    if ($bool_DEBUG) shout('p','sup_brw'); 


#
# ========================================= [ IMPLEMENTATION ]
#


    class BrwActions {
             
        # --- Properties
      
        # Results by page when F action
        private $int_FRESULTSXPAGE = 20;
       
        # Control parameters for action forms 
        private $hash_controlparams = array();
        private $int_init = 0;
        private $str_orderyby = 'name';
        private $str_order = 'asc';
        private $int_total = 0;    
        private $str_filter = ''; // serialized
           
         
        # --- Methods   
            
        # Initialize      
        public function __construct()
            { 
            global $bool_DEBUG, $db, $env, $usr; 
            # --- Create control parameters (!d)  
            $db->dbLogin();
            # services
            $str_query = "SELECT id, name FROM ".$db->dbPrefix()."service ";
            $db->dbQuery($str_query);
            while (list($str_thisID, $str_thisname) = $db->dbFetch()) { 
               if ($env->envParam($str_thisname))   {   
                  $this->hash_controlparams[$str_thisname] = $env->envGet($str_thisname);
                  if ($bool_DEBUG) shout('-',"serv param| $str_thisname = ".$env->envGet($str_thisname)); 
                  }   
               }  
            # subservices
            $str_query = "SELECT id, name FROM ".$db->dbPrefix()."subservice ";
            $db->dbQuery($str_query);
            while (list($str_thisID, $str_thisname) = $db->dbFetch()) {
               if ($env->envParam($str_thisname)) { 
                  $this->hash_controlparams[$str_thisname] = $env->envGet($str_thisname);
                  if ($bool_DEBUG) shout('-',"subs param| $str_thisname = ".$env->envGet($str_thisname));
                  }   
               }  
            $db->dbLogout(); 
            # Add identity if not already 
            if ($usr->usrIs('adm') && !$env->envParam('admin'))
               { $this->hash_controlparams['admin'] = $usr->usrID(); }
            if ($usr->usrIs('usr') && !$env->envParam('user'))
               { $this->hash_controlparams['user'] = $usr->usrID(); }
            if ($bool_DEBUG) shout('?',$this->hash_controlparams); 
    	      }
	

        # List B mode (action_bmode) 
        public function brwListBModes()
            {
            return array (
                array ("value" => 's', "id" => 'bmode_s', "name" => 'bmode', "class" => "label", "text" => 'ACTION_BSECURE', "classtext" => 'label', "checked" => 1 ),  
                array ("value" => 'p', "id" => 'bmode_p', "name" => 'bmode', "class" => "label", "text" => 'ACTION_BPURGE', "classtext" => 'label'), 
                array ("value" => 'r', "id" => 'bmode_r', "name" => 'bmode', "class" => "label", "text" => 'ACTION_BRECURSIVE', "classtext" => 'label'),
	             ); 
            }
          
        # Initialize F parameters    
        public function brwInitFMode($int_total=0) # not autodetected to enable set conditional query
            {
            global $bool_DEBUG, $env;
            # capture and correct values 
            $int_init = ($env->envGet('init') >= 0) ? $env->envGet('init') : 0;
				if (($int_total) && ($int_init > $int_total)) { $int_init = $int_total - 1; }
				elseif (!$int_total) { $int_init = 0; } 
				$str_orderby = ($env->envGet('orderby')) ? preg_replace('/\W/','',$env->envGet('orderby')) : 'name'; 
				$str_order = ($env->envGet('order') != 'desc') ? 'ASC' : 'DESC';
            # store values
            $this->int_init = $int_init;
				$this->str_orderby = $str_orderby;
				$this->str_order = $str_order;
            $this->int_total = $int_total;
            $this->str_filter = ($env->envGet('filter')) ? htmlspecialchars_decode($env->envGet('filter')) : '';
				# validate against '0'
				if (!$int_init) $int_init = '0';
				if (!$int_total) $int_total = '0'; 
				# return
            if ($bool_DEBUG) shout('',"total:$int_total, init:$int_init, resultsxpage:".$this->int_FRESULTSXPAGE.", orderby:$str_orderby, order:$str_order");
            return array($int_init, $this->int_FRESULTSXPAGE, $str_orderby, $str_order);
            }
 
        # Links to page results 
        public function brwListFPages($ah_params) # not autodetected to enable set conditional query
            {
            global $bool_DEBUG, $env;
            # 
            $ah_result = array();   
            # --- Make list
            if ($this->int_total > $this->int_FRESULTSXPAGE) {
                # Recover values for easy manipulation
                $int_init = $this->int_init;
                $str_orderby = $this->str_orderby;
                $str_order = $this->str_order; 
                $int_total = $this->int_total;
                $str_filter = $this->str_filter;
                $int_FRESULTSXPAGE = $this->int_FRESULTSXPAGE;
                if ($bool_DEBUG) shout('',"init:$int_init, orderby:$str_orderby, order:$str_order, total:$int_total, filter:$str_filter, resultsxpage:$int_FRESULTSXPAGE, ah_params:".count($ah_params));
                # Set actual values
                $int_myinit = floor($int_init/$int_FRESULTSXPAGE) * $int_FRESULTSXPAGE;
                $int_mypage = floor($int_init/$int_FRESULTSXPAGE) + 1;
                $int_mylastinit = floor($int_total/$int_FRESULTSXPAGE) * $int_FRESULTSXPAGE;
                $int_mylastpage = floor($int_total/$int_FRESULTSXPAGE) + 1;
                if ($bool_DEBUG) shout('',"init:$int_myinit, page:$int_mypage, lastinit:$int_mylastinit, lastpage:$int_mylastpage  <br/>");
                # Set first, prev 5, prev, actual, next, next 5, last data 
                if ($int_myinit) {
                    if ($bool_DEBUG) shout('',"first: ($int_myinit) | page: 1, init:0 ");
                    $ah_result[] = array(
                        "name" => "ACTION_FFIRST",
                        "page" => 1,
                        "init" => 0,
                        "icon" => "sup/icons/list_first.png",
                        "params" => array_merge($ah_params,
                            array("init"=>0, "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    } 
                if (0 <= ($int_myinit - (5 * $int_FRESULTSXPAGE))) {
                    if ($bool_DEBUG) shout('',"rwd: 0 &lt;= ".($int_myinit - (5 * $int_FRESULTSXPAGE))." | page: ".($int_mypage - 5).", init: ".($int_myinit - (5 * $int_FRESULTSXPAGE)) );
                    $ah_result[] = array(
                        "name" => "ACTION_FRWD",
                        "page" => $int_mypage - 5,
                        "init" => $int_myinit - (5 * $int_FRESULTSXPAGE),
                        "icon" => "sup/icons/list_rwd.png",
                        "params" => array_merge($ah_params,
                            array("init"=>($int_myinit - (5 * $int_FRESULTSXPAGE)), "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    } 
                if (0 <= ($int_myinit - $int_FRESULTSXPAGE)) { // previous
                    if ($bool_DEBUG) shout('',"prev: 0 &lt;= ".($int_myinit - $int_FRESULTSXPAGE)." | page : ".($int_mypage - 1).", init: ".($int_myinit - $int_FRESULTSXPAGE) );
                    $ah_result[] = array(
                        "name" => "ACTION_FPREV",
                        "page" => $int_mypage - 1,
                        "init" => $int_myinit - $int_FRESULTSXPAGE,
                        "icon" => "sup/icons/list_prev.png",
                        "params" => array_merge($ah_params,
                            array("init"=>($int_myinit - $int_FRESULTSXPAGE), "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    }
                if ($int_mylastinit)	{ // if lastinit = 0 then nothing to browse
                    if ($bool_DEBUG) shout('',"actual: $int_mylastinit | page : $int_mypage , init: $int_myinit ");	
                    $ah_result[] = array(
                        "name" => "ACTION_FACTUAL",
                        "page" => $int_mypage,
                        "init" => $int_myinit,
                        "icon" => "sup/icons/list_actual.png" 
                        );						
                    } 
                if (($int_myinit + $int_FRESULTSXPAGE) <= $int_mylastinit) { // next
                    if ($bool_DEBUG) shout('',"next: ".($int_myinit + $int_FRESULTSXPAGE)." &lt;= $int_mylastinit | page: ".($int_mypage + 1)." , init: ".($int_myinit + $int_FRESULTSXPAGE) );
                    $ah_result[] = array(
                        "name" => "ACTION_FNEXT",
                        "page" => $int_mypage + 1,
                        "init" => $int_myinit + $int_FRESULTSXPAGE,
                        "icon" => "sup/icons/list_next.png",
                        "params" => array_merge($ah_params,
                            array("init"=>($int_myinit + $int_FRESULTSXPAGE), "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    }  
                if (($int_myinit + (5 * $int_FRESULTSXPAGE)) < $int_mylastinit) {
                    if ($bool_DEBUG) shout('',"fwd: ".($int_myinit + (5 * $int_FRESULTSXPAGE))." &lt; $int_mylastinit | page: ".($int_mypage + 5)." , init: ".($int_myinit + (5 * $int_FRESULTSXPAGE)) );
                    $ah_result[] = array(
                        "name" => "ACTION_FFWD",
                        "page" => $int_mypage + 5,
                        "init" => $int_myinit + (5 * $int_FRESULTSXPAGE),
                        "icon" => "sup/icons/list_fwd.png",
                        "params" => array_merge($ah_params,
                            array("init"=>($int_myinit + (5 * $int_FRESULTSXPAGE)), "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    } 
                if ($int_myinit < $int_mylastinit) {
                    if ($bool_DEBUG) shout('',"last: $int_myinit &lt; $int_mylastinit | page: $int_mylastpage , init: $int_mylastinit ");
                    $ah_result[] = array(
                        "name" => "ACTION_FLAST",
                        "page" => $int_mylastpage,
                        "init" => $int_mylastinit,
                        "icon" => "sup/icons/list_last.png",
                        "params" => array_merge($ah_params,
                            array("init"=>$int_mylastinit, "orderby"=>$str_orderby, "order"=>$str_order, "filter"=>$str_filter, "action"=>"f")
                            )
                        );
                    } 
               } 
            return $ah_result;
            } 

          
        # Get control parameters    
        public function brwBasicControl()  
            { 
            return $this->hash_controlparams;
            } 

        }

?>