<?php

# ============================================================
#    SiteUP!:   (c)  J. Alejandro Ceballos Z.
#       Name:   sup_env.php - Build 2009
#    License:   MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
    if (preg_match("/sup_env/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
    if ($bool_DEBUG) shout('p','sup_env'); 

 
#
# ========================================= [ IMPLEMENTATION ]
# 

    class EnvData {
        
        # Network data 
        private $str_FULLDOMAIN, $str_IP; 
    
        # Directories 
        private $str_ROOT, $str_TMP, $str_HTDOCS, $str_HOME, $str_CGIDIR, $str_TEMP;
              
        # SiteUP data
        # db data
        private $str_LANG, $arr_AVAILABLELANGS, $arr_AVAILABLEACTIONS, $hash_default;
        # parameter data 
        private $hash_page, $hash_service, $hash_subservice, $char_action;       
           
           
        # --- Methods   
            
        # Initialize      
        public function __construct()
            {
            global $bool_DEBUG, $db; 
            
            # -- Network data
        
            # local host
            $this->str_FULLDOMAIN = strtolower($_SERVER["HTTP_HOST"]);
           
            # remote ip
            $this->str_IP = $_SERVER["REMOTE_ADDR"];
            
            if ($bool_DEBUG) shout('',"network data: localhost:$this->str_FULLDOMAIN, remote ip:$this->str_IP"); 
                
            # -- Directories
        
            # recover actual path 
            preg_match('/^(.+)public_html/',getcwd(),$arr_matches);
        
            # physical path to user home (~user; usually private servers:/var/www/ , shared servers:/home/user/ ) 
            $this->str_ROOT = ($arr_matches[1]) ? $arr_matches[1] : '/var/www/';
           
            # physical hidden temporal path directory (usually private servers:/tmp/ , shared servers:/home/user/tmp/ ) 
            $this->str_TMP = $this->str_ROOT.'tmp/';
           
            # physical htdocs directory (usually private servers:/var/www/html/ , shared servers:/home/user/public_html/ )  
            $this->str_HTDOCS = $this->str_ROOT.'public_html/';
           
            # logical home (usually /)
            $this->str_HOME = '/';
           
            # logical cgi scripts (usually wamp servers: scripts/ , lamp servers: cgi-bin/ )
            $this->str_CGIDIR = $this->str_HOME.'cgi-bin/';  
           
            # logical visible local temp directory (usually private servers:/var/www/html/temp/ , shared servers:/home/user/public_html/temp/ )
            $this->str_TEMP = $this->str_HOME.'temp/';
           
            if ($bool_DEBUG) shout('',"directories: root:$this->str_ROOT, tmp:$this->str_TMP, htdocs:$this->str_HTDOCS, home:$this->str_HOME,  cgi:$this->str_CGIDIR, temp:$this->str_TEMP ");  
            
            # -- SiteUP config data from DB
    
            # init
            $db->dbLogin();
            
            # all defaults
            $db->dbQuery("SELECT * FROM ".$db->dbPrefix()."config ");   
            while (list($str_thisID, $str_thisname) = $db->dbFetch())
               { $this->hash_default[$str_thisID] = $str_thisname; }   
            if ($bool_DEBUG) { shout('%',$this->hash_default); }
            
            # available lang (if no lang is desired, just not include dictionary) 
            $this->str_AVAILABLELANGS = join(' ',array_intersect($arr_this,explode(' ',$this->hash_default["lang"])));  
            $this->str_LANG = preg_replace('/\s.*/','',$this->str_AVAILABLELANGS);
            if ($bool_DEBUG) { shout('-',"langs:".$this->str_AVAILABLELANGS.", lang:".$this->str_LANG); } 
            
            # -- SiteUP! service / page 

            # get service info
            if ($this->envParam('service')) {
                list($this->hash_service["ID"], $this->hash_service["servedID"]) = (is_array($this->envParam('service'))) ? $this->envParam('service') : array($this->envParam('service'),'');
                $this->hash_service["ID"] = preg_replace('/\W/','',$this->hash_service["ID"]);
                $db->dbQuery("SELECT id, code, name, privileged, usractions FROM ".$db->dbPrefix()."service WHERE id = '".$this->hash_service["ID"]."' AND (active = '1') LIMIT 1 ");
                list ($this->hash_service{"ID"}, $this->hash_service{"code"}, $this->hash_service{"name"}, $this->hash_service{"privileged"}, $this->hash_service{"usractions"}) = $db->dbFetch();   
                if ($this->hash_service["ID"]) {
                    $this->hash_service["servedID"] = ($this->hash_service["servedID"]) ? $this->hash_service["servedID"] : $this->envParam('code');
                    $this->hash_service["servedID"] = preg_replace('/\W/','',$this->hash_service["servedID"]);
                    if ($bool_DEBUG) shout('%',$this->hash_service);
                    # get subservice info 
                    if ($this->envParam('subservice')) {
                        list($this->hash_subservice["ID"], $this->hash_subservice["servedID"]) = (is_array($this->envParam('subservice'))) ? $this->envParam('subservice') : array($this->envParam('subservice'),'');
                        $this->hash_subservice["ID"] = preg_replace('/\W/','',$this->hash_subservice["ID"]);
                        $db->dbQuery("SELECT id, code, name, privileged, usractions FROM ".$db->dbPrefix()."subservice WHERE id = '".$this->hash_subservice["ID"]."' AND (active = '1') LIMIT 1 ");
                        list ($this->hash_subservice{"ID"}, $this->hash_subservice{"code"}, $this->hash_subservice{"name"}, $this->hash_subservice{"privileged"}, $this->hash_subservice{"usractions"}) = $db->dbFetch();   
                        if ($this->hash_subservice["ID"]) {
                            $this->hash_subservice["servedID"] = ($this->hash_subservice["servedID"]) ? $this->hash_subservice["servedID"] : $this->envParam('code');
                            $this->hash_subservice["servedID"] = preg_replace('/\W/','',$this->hash_subservice["servedID"]);
                            if ($bool_DEBUG) shout('%',$this->hash_subservice);
                            }
                        }
                    }
                if ($this->envParam('action'))
                    { $this->char_action = preg_replace('/\W/','',$this->envParam('action')); } 
                } 
            elseif (param('page')) {
                $this->hash_page["ID"] = preg_replace('/\W/','',$this->hash_page["ID"]);
                $db->dbQuery("SELECT id, code, name, privileged, usractions FROM ".dbPrefix()."page WHERE (id = '".$this->hash_page["ID"]."') AND (active = '1') LIMIT 1 ");               
                list($this->hash_page["ID"], $this->hash_page["code"], $this->hash_page["name"], $this->hash_page["privileged"], $this->hash_page["usractions"]) = $db->dbFetch(); 
                if ($bool_DEBUG) shout('%',$this->hash_page);
                }
                
            # ends 
            dbLogout();
            }
             
  
        # Recover directory 
        public function envDir($k='') 
            {
            switch ($k) { 
                case 'root': return $str_ROOT; 
                case 'tmp': return $str_TMP;  
                case 'htdocs': return $str_HTDOCS;  
                case 'home': return $str_HOME; 
                case 'cgi': return $str_CGIDIR;  
                case 'temp': return $str_TEMP; 
                default: return './';
                }   
            }
         
      # Return generic value: variables > default > param        
        public function envGet($k='')  
            {
            switch ($k) {
                case 'ip': return $this->str_IP;  
                case 'fulldomain': return $this->str_FULLDOMAIN; 
                case 'lang': return $this->str_LANG; 
                # security code (generated during installation; must match with sup_env.php)
                case 'seccode': return '#SECCODE#';  
                default: return ($this->hash_default[$k]) ? $this->hash_default[$k] : '';
                }
            }
             
        # Type of information to load         
        public function envIs($k='')     
            {   
            switch ($k) {
                case 'page': return ($this->hash_page['ID']) ? 1 : 0;  
                case 'service': return ($this->hash_service['ID']) ? 1 : 0;  
                case 'subservice': return ($this->hash_subservice['ID']) ? 1 : 0;  
                }      
            } 
       
        # Page information           
        public function envPage($k='')     
            {   
            switch ($k) {
                case 'id': return $this->hash_page['ID'];    
                case 'code': return $this->hash_page['code'];  
                case 'name': return $this->hash_page['name'];  
                case 'privileged': return $this->hash_page['privileged'];  
                case 'usractions': return $this->hash_page['usractions'];  
                } 
            }      
          
        # Service information           
        public function envServ($k='')    
            {   
            switch ($k) {
                case 'id': return $this->hash_service['ID'];  
                case 'servedid': return $this->hash_service['servedID'];  
                case 'code': return $this->hash_service['code'];  
                case 'name': return $this->hash_service['name'];  
                case 'privileged': return $this->hash_service['privileged'];  
                case 'usractions': return $this->hash_service['usractions'];  
                } 
            }            
            
        # Service information           
        public function envSubs($k='')     
            {   
            switch ($k) {
                case 'id': return $this->hash_subservice['ID'];  
                case 'servedid': return $this->hash_subservice['servedID'];  
                case 'code': return $this->hash_subservice['code'];  
                case 'name': return $this->hash_subservice['name'];  
                case 'privileged': return $this->hash_subservice['privileged'];  
                case 'usractions': return $this->hash_subservice['usractions'];  
                } 
            }     
                
        # Return default value 
        public function envDefault($k='')  
            { return ($k) ? $this->hash_default[$k] : $this->hash_default; }  
 
        # Return param value      
        public function envParam($k='')
            { return ($_REQUEST[$k] && !$_COOKIE[$k]) ? $_REQUEST[$k] : FALSE; } 
            
        # Return cookie value      
        public function envCookie($k='')
            { return ($_COOKIE[$k] && !$_REQUEST[$k]) ? $_COOKIE[$k] : FALSE; } 
            
        }

        
?>