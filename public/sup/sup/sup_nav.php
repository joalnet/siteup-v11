<?php

# ============================================================
#    SiteUP!:   (c)  J. Alejandro Ceballos Z.
#       Name:   sup_nav.php - Build 1508
#    License:   MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
   if (preg_match("/sup_nav/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) shout('p','sup_nav'); 


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

   # Previous load by index
   # require_once("debug.php");
   # require_once("sup_db.php");
   # require_once("sup_user.php");
   # require_once("sup_env.php");
 

#
# ========================================= [ IMPLEMENTATION ]
#


   class NavObject   {
            
      # --- Properties
      
      # All services available (for specific AUTHOR)
         private $hash_services = array();
      # Current service   
         private $str_serviceID = ''; 
         private $str_ID = '';
      # All subservices available (for specific AUTHOR and SERVICE)
         private $hash_subservices = array();
      # Current subservice
         private $str_subserviceID = ''; 
         private $str_subID = '';
      # Current action
         private $char_action = '';


      # --- Methods (private)
      
      public function __construct()
         { 
         global $bool_DEBUG, $db, $env, $usr;
         #  
         # Recover data related to all services
         if ($bool_DEBUG) { shout('-',"all services"); }
         $db->dbLogin();  
         $str_query = "SELECT id, name FROM ".$db->dbPrefix()."service ";
         $str_query .= ($usr->usrIs('adm')) ? " WHERE (LENGTH(admactions) > 0) " : " WHERE (LENGTH(usractions) > 0) AND (active = '1') ";
         $str_query .= "ORDER BY privileged DESC, name ";   
         $db->dbQuery($str_query);   
         while (list($str_thisID, $str_thisname) = $db->dbFetch('row'))
            { $this->hash_services[$str_thisID] = $str_thisname; } 
         if ($bool_DEBUG) { shout('%',$this->hash_services); }
         #
         # Recover data related to actual service
         if ($env->envGet("service")) { 
            if (is_array($env->envGet("service"))) {
	      if ($bool_DEBUG) shout('',"service is array");
               list($this->str_serviceID, $this->str_ID) = $env->envGet("service"); 
               }
            else {
	      if ($bool_DEBUG) shout('',"service is NOT array");
               $this->str_serviceID = $env->envGet("service"); 
               $this->str_ID = ($env->envGet('servicename') != 'service') ? $env->envGet($env->envGet('servicename')) : '';
               }
	   if ($bool_DEBUG) shout('',"service:".$this->str_serviceID.", servicename:".$this->str_servicename.", id:".$this->str_ID); 
            # data associated to current service
            $str_query = "SELECT privileged, active, admactions, usractions FROM ".$db->dbPrefix()."service WHERE (id = '$this->str_serviceID') LIMIT 1 "; 
            $db->dbQuery($str_query);   
            list($bool_privileged, $bool_active, $str_admactions, $str_usractions) = $db->dbFetch('row');
            $this->hash_servicedata = array("privileged"=>$bool_privileged, "active"=>$bool_active, "admactions"=> ($usr->usrIs('adm')) ? $str_admactions : '', "usractions"=>$str_usractions);
            #
            # Recover data all subservices asociated to actual service
            if ($bool_DEBUG) { shout('-',"subservices associated to ".$this->str_servicename ); }
            $str_query = "SELECT id, name FROM ".$db->dbPrefix()."subservice ";
            $str_query .= ($usr->usrIs('adm')) ? " WHERE (LENGTH(admactions) > 0) " : " WHERE (LENGTH(usractions) > 0) AND (active = '1') ";
            $str_query .= " AND (service_id = '$this->str_serviceID') ORDER BY privileged DESC, name "; 
            $db->dbQuery($str_query);   
            while (list($str_thisID, $str_thisname) = $db->dbFetch('row'))
               { $this->hash_subservices[$str_thisID] = $str_thisname; }
            if ($bool_DEBUG) { shout('%',$this->hash_subservices); }   
            #
            # Recover data related to actual subservice
            if ($env->envGet("subservice")) {
               if ($bool_DEBUG) { shout('-',"subservices found"); } 
               if (is_array($env->envGet("subservice"))) {
		if ($bool_DEBUG) shout('',"subservice is array"); 
                  list($this->str_subserviceID, $this->str_subID) = $env->envGet("subservice"); 
                  }
               else {
		if ($bool_DEBUG) shout('',"service is NOT array");
                  $this->str_subserviceID = $env->envGet("subservice"); 
                  $this->str_subID =  ($env->envGet('subservicename') != 'subservice') ? $env->envGet($env->envGet('subservicename')) : '';
                  }
	      if ($bool_DEBUG) shout('',"subservice:".$this->str_subserviceID.", servicename:".$this->str_subservicename.", id:".$this->str_subID);
               # data associated to current subservice
               $str_query = "SELECT service_id, privileged, active, admactions, usractions FROM ".$db->dbPrefix()."subservice WHERE (id = '".$this->str_subserviceID."') LIMIT 1 "; 
               $db->dbQuery($str_query);   
               list($str_parentID, $bool_privileged, $bool_active, $str_admactions, $str_usractions) = $db->dbFetch('row');
               $this->hash_subservicedata = array("parentID"=>$str_parentID, "privileged"=>$bool_privileged, "active"=>$bool_active, "admactions"=> ($usr->usrIs('adm')) ? $str_admactions : '', "usractions"=>$str_usractions);
               }
            }
         $db->dbLogout();
         # Get action
         $this->char_action = $env->envGet("action");
         #
         if ($bool_DEBUG) {
            shout('',"serviceID: $this->str_serviceID, ID:$this->str_ID");
            shout('?',$this->hash_services);
            shout('',"subserviceID: $this->str_subserviceID, subID:$this->str_subID");
            shout('?',$this->hash_subservices);
            shout('',"action:$this->char_action");
            }  
         }


      # --- Methods (public)
      
      # Create list items
         public function navListItems() 
            { 
            global $bool_DEBUG, $usr, $env; 
   	   #
   	   $ah_this = array();
            # type (serv|subs|action), path (bool), selected (bool), link (str), name (str),
            if ($bool_DEBUG) shout('f',"navListItems "); 
            #
            if ($bool_DEBUG) shout('',"USER | privileged:".$usr->usrIs('privileged'));
            # FOREACH SERVICES
            if ($bool_DEBUG) shout('',"foreach SERV");
            foreach ($this->hash_services as $str_thisID => $str_thisname) {
               if ($usr->usrIs('privileged') >= $this->hash_service["privileged"]) { 
                  # add current service
                  $bool_thispath = ($this->str_serviceID == $str_thisID) ? 1:0;
                  $bool_thisselected = (($this->str_serviceID == $str_thisID) && (!$this->str_subserviceID) && (!$this->char_action)) ? 1:0;
                  $str_thisclass = ($bool_thisselected) ? 'nav_active' : (($bool_thispath) ? 'nav_path' : '');
                  $ah_this[] = array(
                        "type"=>"serv", 
                        "class" => "nav_lvl1 $str_thisclass",
                        "href" => "index.php",
                        "params" => array("service"=>$str_thisID),
                        "name" => strtoupper($str_thisname),
   		      "icon" => "sup/mod/$str_thisname/icon.png"
                        );
   	      if ($bool_DEBUG) shout('p'," SERV added | id:$str_thisID, name:$str_thisname, privileged:".$this->hash_services["privileged"].". | ah_total :".count($ah_this));
                  # Display actions if is actual service and there are actions
                  if ($this->str_serviceID == $str_thisID) {
                     # current service
   		if ($bool_DEBUG) shout('',"SERV current"); 
                     if (!$this->str_subserviceID) {
                        # display action if no subservice selected
                        if ($bool_DEBUG) shout('',"SERV actions (not SUBS selected)");
                        $str_thisservactions = ($usr->usrIs('adm')) ? $this->hash_servicedata["admactions"] : $this->hash_servicedata["usractions"];
                        if ($bool_DEBUG) shout('',"foreach SERV ACTIONS");
                        foreach (str_split($str_thisservactions) as $str_thisaction) {
                           if (preg_match('/[afh]/',$str_thisaction) || $this->str_ID) {
      		            $bool_thispath = ((!$this->str_subserviceID) && ($this->char_action) && ($this->char_action == $str_thisaction)) ? 1:0;
                              $bool_thisselected = ($this->char_action == $str_thisaction) ? 1:0; 
                              $str_thisclass = ($bool_thisselected) ? 'nav_active' : (($bool_thispath) ? 'nav_path' : ''); # !$bool_thisenabled => 'nav_disabled' 
                              $ah_this[] = array(
                                 "type"=>"action",
                                 "class" => "nav_lvl3 $str_thisclass",
                                 "href" => "index.php",
                                 "params" => (preg_match('/[afh]/',$str_thisaction)) 
                                    ? array("service"=>$this->str_serviceID, "action"=>$str_thisaction) 
                                    : array("service"=>$this->str_serviceID, $env->envGet('servicename')=>$this->str_ID, "action"=>$str_thisaction),
                                 "name" => strtoupper('action_'.$str_thisaction),
   			      "icon" => "sup/icons/action_$str_thisaction.png"
                                 );
      		            if ($bool_DEBUG) { shout('-',"ACTION added | action:$str_thisaction | ah_total :".count($ah_this)); }                           
                              } 
                           }
                        } 
                     # FOREACH SUBSERVICES
                     if ($bool_DEBUG) shout('',"foreach SUBS");
                     foreach ($this->hash_subservices as $str_thissubID => $str_thissubname) {
                        if ($usr->usrIs('privileged') >= $this->hash_subservice["privileged"]) { 
                           # add current subservice (no privilige validation due it is relative to the parent service)
      		         $bool_thispath = ($this->str_subserviceID == $str_thissubID) ? 1:0;
                           $bool_thisselected = (($this->str_subserviceID == $str_thissubID) && (!$this->char_action)) ? 1:0;
                           $str_thisclass = ($bool_thisselected) ? 'nav_active' : (($bool_thispath) ? 'nav_path' : '');  
                           $ah_this[] = array(
                                 "type"=>"subs", 
                                 "class" => "nav_lvl2 $str_thisclass",
                                 "href" => "index.php",
                                 "params" => array("service"=>$this->str_serviceID, "subservice"=>$str_thissubID), 
                                 "name" => strtoupper($str_thissubname),
      			      "icon" => "sup/mod/$str_thisname/$str_thissubname.png"
                                 );
      		      if ($bool_DEBUG) { shout('-',"SUBS added | id:$str_thisID, name:$str_thisname, privileged:".$this->hash_services["privileged"].". | ah_total:".count($ah_this)); }
                           # Display actions if is actual subservice and there are actions
                           if ($this->str_subserviceID == $str_thissubID) {
      		         if ($bool_DEBUG) shout('-',"SUBS current");
                              $str_thissubservactions = ($usr->usrIs('adm')) ? $this->hash_subservicedata["admactions"] : $this->hash_subservicedata["usractions"]; 
                              if ($bool_DEBUG) shout('',"foreach SUBS ACTIONS"); 
                              foreach (str_split($str_thissubservactions) as $str_thisaction) {
                                 if (preg_match('/[afh]/',$str_thisaction) || $this->str_subID) {
      		                  $bool_thispath = ($this->char_action == $str_thisaction) ? 1:0;
                                    $bool_thisselected = ($this->char_action == $str_thisaction) ? 1:0;
                                    $str_thisclass = ($bool_thisselected) ? 'nav_active' : (($bool_thispath) ? 'nav_path' : '');  
                                    $ah_this[] = array(
                                       "type"=>"action", 
                                       "class" => "nav_lvl3 $str_thisclass",
                                       "href" => "index.php",
                                       "params" => (preg_match('/[af]/',$str_thisaction)) // no h
                                          ? array("service"=>$this->str_serviceID, "subservice"=>$this->str_subserviceID, "action"=>$str_thisaction) 
                                          : array("service"=>$this->str_serviceID, "subservice"=>$this->str_subserviceID, $env->envGet('subservicename')=>$this->str_subID, "action"=>$str_thisaction), 
                                       "name" => strtoupper('action_'.$str_thisaction),
      				   "icon" => "sup/icons/action_$str_thisaction.png"
                                       );
      		                  if ($bool_DEBUG) { shout('-',"SUBS ACTION added | ah_total: ".count($ah_this)); } 
                                    }    
                                 } 
                              } 
                           } # if subs privilege >
                        } # foreach SUBS 
                     } # servID = 
                  }  # if serv privilege >
               }  # foreach SERV
            # add logout
            $ah_this[] = array(
                  "type"=>"log", 
                  "class" => "nav_lvl1 ", 
                  "href" => $env->envDir('cgi').'logout.cgi',
                  "params" => '',
                  "name" => 'LOGOUT',
   	         "icon" => "sup/icons/login_onoff.png"
                  );
            if ($bool_DEBUG) { shout('-',"LOG added | ah_total:".count($ah_this)); } 
            # 
            return $ah_this;   
            } 
    
    
      # Get list of available actions for current service (NOTE != envActions('available'))
         public function navGet ($k='')
            {
            switch ($k) {
               case 'servadmactions':
                  return $this->hash_service["admactions"]; 
               case 'servusractions':
                  return $this->hash_service["usractions"]; 
               case 'subsadmactions':
                  return $this->hash_subservice["admactions"]; 
               case 'subsusractions':
                  return $this->hash_subservice["usractions"]; 
               default:
                  return '';
               }
            }
 
 
      }   
         
 
?>