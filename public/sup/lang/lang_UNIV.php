<?php

/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  lang_UNIV.php - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */

# Block direct access
   if (preg_match("/lang_UNIV/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) shout('p','lang_UNIV'); 


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Constants ]


   # Language codes - ISO 639-1
   
   $hash_LANG = array( 
      "EN" => "english", 
      "ES" => "español" 
      );
    
    
   # Country codes - ISO 3166 Alpha-2
   
   $hash_COUNTRY = array(  
      "AR" => "Argentina",  
      "AU" => "Australia",  
      "BZ" => "Belize",  
      "BO" => "Bolivia",  
      "BR" => "Brasil",  
      "CA" => "Canada",  
      "CH" => "Schweiz",  
      "CL" => "Chile",  
      "CN" => "中华人民共和国",  
      "CO" => "Colombia",  
      "CR" => "Costa Rica",  
      "DE" => "Deutschland",  
      "DO" => "República Dominicana",  
      "EC" => "Ecuador",  
      "ES" => "España",  
      "FR" => "France",  
      "GB" => "United Kingdom",  
      "GR" => "Ελλάδα",  
      "GT" => "Guatemala",  
      "HK" => "香港",  
      "HN" => "Honduras",  
      "IL" => "מדינת ישראל",  
      "IN" => "भारत गणराज्य",  
      "JP" => "日本",  
      "MX" => "México",  
      "NI" => "Nicaragua",  
      "PA" => "Panama",  
      "PE" => "Peru",  
      "PT" => "Portugal",  
      "PY" => "Paraguay",  
      "RO" => "România",  
      "RU" => "Россия",  
      "SE" => "Sverige",  
      "SV" => "El Salvador",  
      "UA" => "Україна",  
      "US" => "United States of America",  
      "UY" => "Uruguay",  
      "VE" => "Venezuela", 
      "ZA" => "Republiek van Suid-Afrika" 
      );
  
  
   # Subdivision codes - ISO 3166-2 (alfa 3)
   
   $hash_REGION["MX"] = array(  
      "MX-AGU" => 'Aguascalientes', 
      "MX-BCN" => 'Baja California', 
      "MX-BCS" => 'Baja California Sur', 
      "MX-CAM" => 'Campeche', 
      "MX-CHP" => 'Chiapas', 
      "MX-CHH" => 'Chihuahua', 
      "MX-COA" => 'Coahuila', 
      "MX-COL" => 'Colima', 
      "MX-CMX" => 'Ciudad de México', 
      "MX-DUR" => 'Durango', 
      "MX-GUA" => 'Guanajuato', 
      "MX-GRO" => 'Guerrero', 
      "MX-HID" => 'Hidalgo', 
      "MX-JAL" => 'Jalisco', 
      "MX-MEX" => 'México', 
      "MX-MIC" => 'Michoacán', 
      "MX-MOR" => 'Morelos', 
      "MX-NAY" => 'Nayarit', 
      "MX-NLE" => 'Nuevo León', 
      "MX-OAX" => 'Oaxaca', 
      "MX-PUE" => 'Puebla', 
      "MX-QTO" => 'Querétaro', 
      "MX-ROO" => 'Quintana Roo', 
      "MX-SLP" => 'San Luis Potosí', 
      "MX-SIN" => 'Sinaloa', 
      "MX-SON" => 'Sonora', 
      "MX-TAB" => 'Tabasco', 
      "MX-TAM" => 'Tamaulipas', 
      "MX-TLA" => 'Tlaxcala', 
      "MX-VER" => 'Veracruz-Llave', 
      "MX-YUC" => 'Yucatán', 
      "MX-ZAC" => 'Zacatecas'
      );
   
   $hash_REGION["US"] = array(       
      "US-AL" => 'Alabama', 
      "US-AK" => 'Alaska', 
      "US-AZ" => 'Arizona', 
      "US-AR" => 'Arkansas', 
      "US-CA" => 'California', 
      "US-CO" => 'Colorado', 
      "US-CT" => 'Connecticut', 
      "US-DE" => 'Delaware', 
      "US-FL" => 'Florida', 
      "US-GA" => 'Georgia', 
      "US-HI" => 'Hawaii', 
      "US-ID" => 'Idaho', 
      "US-IL" => 'Illinois', 
      "US-IN" => 'Indiana', 
      "US-IA" => 'Iowa', 
      "US-KS" => 'Kansas', 
      "US-KY" => 'Kentucky', 
      "US-LA" => 'Louisiana', 
      "US-ME" => 'Maine', 
      "US-MD" => 'Maryland', 
      "US-MA" => 'Massachusetts', 
      "US-MI" => 'Michigan', 
      "US-MN" => 'Minnesota', 
      "US-MS" => 'Mississippi', 
      "US-MO" => 'Missouri', 
      "US-MT" => 'Montana', 
      "US-NE" => 'Nebraska', 
      "US-NV" => 'Nevada', 
      "US-NH" => 'New Hampshire', 
      "US-NJ" => 'New Jersey', 
      "US-NM" => 'New Mexico', 
      "US-NY" => 'New York', 
      "US-NC" => 'North Carolina', 
      "US-ND" => ' North Dakota', 
      "US-OH" => 'Ohio', 
      "US-OK" => 'Oklahoma', 
      "US-OR" => 'Oregon', 
      "US-PA" => 'Pennsylvania', 
      "US-RI" => 'Rhode Island', 
      "US-SC" => 'South Carolina', 
      "US-SD" => 'South Dakota', 
      "US-TN" => 'Tennessee', 
      "US-TX" => 'Texas', 
      "US-UT" => 'Utah', 
      "US-VT" => 'Vermont', 
      "US-VA" => 'Virginia', 
      "US-WA" => 'Washington', 
      "US-WV" => 'West Virginia', 
      "US-WI" => 'Wisconsin', 
      "US-WY" => 'Wyoming', 
      "US-DC" => 'District of Columbia', 
      "US-AS" => 'American Samoa', 
      "US-GU" => 'Guam', 
      "US-MP" => 'Northern Mariana Islands', 
      "US-PR" => 'Puerto Rico', 
      "US-UM" => 'United States Minor Outlying Islands', 
      "US-VI" => 'Virgin Islands, U.S.'
      );
  
  
   # Currency codes - ISO 4217
   
   $hash_CUR = array( 
      "USD" => "US Dollar",  
      "MXP" => "Peso mexicano",
      "EUR" => "Euro" 
      );
    

?>