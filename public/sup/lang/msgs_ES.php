<?php

/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  msgs_ES.php - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */

# Block direct access
   if (preg_match("/msgs_ES/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) shout('p','msgs_ES'); 


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Constants ]

  $hash_BOOLEAN = array("0" => "No ", "1" => "Si ");

  $hash_MESSAGE = array( 
      # Informative 
      "jsrequired" => "Favor de habilitar cookies y javascript en su navegador. ",  
      "chgpwd" => "SEGURIDAD COMPROMETIDA. Favor de cambiar inmediatamente su contraseña. ", 
      "error" => "ERROR ",
      "ok" => "OPERACION REALIZADA ", 
      # Errors 
      "corrupted" => "Datos inconsistentes. ",
      "datarequired" => "Campos incompletos. Favor de revisar. ", 
      "invalidaction" => "Acción no soportada. ",
      "incompletereq" => "Requerimientos incompletos",
      "predefinedid" => "Identificador predefinido. ",
      "unknownbmode" => "Forma de borrado no reconocida. ",
      "wrongpwd" => "No coinciden clave y contraseña. Favor de checar capitalización. ",
      "securitybreak" => "FALLO DE SEGURIDAD. Favor de cambiar su contraseña de manera inmediata. ",
      "chgpwdok" => "Cambio realizado. Favor de firmarse con nueva clave. ",
      "emailunsent" => "No se ha podido enviar el correo ", 
      "idcorrupted" => "Credenciales de identidad corruptas ",
      "inexistent" => "No existe dicho registro. ", 
      "notemptyrecords" => "Existen datos previos. ",  
      );



  
?>