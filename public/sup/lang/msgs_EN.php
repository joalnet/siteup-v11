<?php

/* ============================================================
    SiteUP!  (c) J. Alejandro Ceballos Z.
      Name:  msgs_EN.php - Build 1506
   License:  MIT - https://tldrlegal.com/license/mit-license 
 ============================================================ */

# Block direct access
   if (preg_match("/msgs_EN/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) shout('p','msgs_EN'); 


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Constants ]

  $hash_BOOLEAN = array("0" => "No ", "1" => "Yes ");
  
  $hash_MESSAGE = array(
      # Informative 
      "jsrequired" => "Please enable Cookies and Javascript in your browser. ",   
      "chgpwdok" => "Password changed. Please sign on with the new one. ",  
      "error" => "ERROR ",
      "ok" => "DONE ",
      # Errors 
      "corrupted" => "Incongruent information. ",
      "datarequired" => "Incomplete data. Please fill all required fields. ", 
      "invalidaction" => "Action not supported. ",
      "incompletereq" => "Incomplete requirements",
      "predefinedid" => "Logical error. Predefined ID. ",
      "unknownbmode" => "Unknown delete mode. ", 
      "wrongpwd" => "Login and password does not match. Please check capitalization. ",  
      "securitybreak" => "SECURITY BREAK. Please change your password immediately. ",
      "chgpwdok" => "Cambio realizado. Favor de firmarse con nueva clave. ",
      "emailunsent" => "An error occurred while sending email ", 
      "idcorrupted" => "Identity credentials corrupted ",
      "inexistent" => "No existe dicho registro. Favor de revisar mayúsculas y minúsculas. ", 
      "notemptyrecords" => "Records on use. ",  
      );

?>