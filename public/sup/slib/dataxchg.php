<?php

# ============================================================
#   SiteUP!:  (c)  J. Alejandro Ceballos Z.
#      Name:  sup_init.php - Build 1607
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
   if (preg_match("/dataxchg/",$_SERVER['PHP_SELF'])) {
      Header("Location: /403.shtml");
      die(); 
      } 
   if ($bool_DEBUG) { Shout('p','dataxchg'); }


 
#
# ========================================= [ FUNCTIONS ]
#

      
      
#
#  Hash (structure arr, hash, ah, ha, aa, hh, ...) <-> XML
#

   #  Auxiliary for hash2xml   
   function hashToXml ($hash_data, &$xml_data)
      {
      # Based on http://pastebin.com/pYuXQWee
      foreach($hash_data as $k => $v) {
         if (is_array($v)) {
            $k = is_numeric($k) ? "item$k" : $k;
            $s = $xml_data->addChild("$k");
            HashToXml($v, $s);
            }
         else {
            $k = is_numeric($k) ? "item$k" : $k;
            $xml_data->addChild("$k","$v");
            }
         }
      }
     
   function hash2Xml ($hash_data, $str_name='data')
      { return hashToXml($hash_data,new SimpleXMLElement("<$str_name/>")); }
     
   function xml2Hash ($xml_data)
      { return simplexml_load_string($xml_data); }
       
      
#
#  ah (array of hashes) -> html structure 
#      
#			ah[] = array(class, id, name, val, text, classtext, icon, link, selected, checked, disabled, readonly );
#
      
   function ah2NavList ($ah_this, $str_id='', $str_name='', $str_class='')
      {
      $str_result = '';	
      foreach ($ah_this as $hash_this) {
   	   $str_thisclass = $hash_this["class"]; 
       	$str_thislink = $hash_this["link"];
   	   $str_thisname = $hash_this["name"]; 
   	   $str_thisicon = $hash_this["icon"]; 
	      $str_result .= "<li class=\"$str_thisclass\"><a href=\"$str_thislink\"><img src=\"$str_thisicon\" class=\"icon label\" alt=\"$str_thisname\" />"
	         . "<span class=\"label\">".strtoupper($str_thisname)."</span></a></li> \n";   
         }
      return $str_result;	
      }  
      
      
   function ah2List ($ah_this, $str_id='', $str_name='', $str_class='')
      {
		# ol
      $str_result = "<ol ";
		if (($str_id) || ($str_name)) $str_result .= ($str_id) ? 'id="'.$str_id.'" ' : 'id="'.$str_name.'" ';
		if ($str_name) $str_result .= "name=\"$str_name\" ";
      if ($str_class) $str_result .= 'class="'.$str_class.'" ';
      $str_result .= "> \n";
		# ah_this
      foreach ($ah_this as $hash_this) {
			# li
			$str_result .= '<li '; 
			if (($hash_this["id"]) || ($hash_this["name"])) $str_result .= ($hash_this['id']) ? 'id="'.$hash_this["id"].'" ' : 'id="'.$hash_this["name"].'" ';
			if ($hash_this["name"]) $str_result .= 'name="'.$hash_this["name"].'" '; 
			if ($hash_this["class"]) $str_result .= 'class="'.$hash_this["class"].'" ';
			$str_result .= '>';
			# li contents
			$str_thisname = ($hash_this["name"]) ? $hash_this["name"] : '';
         if ($hash_this["icon"]) $str_result .= "<img src=\"$str_thisicon\" class=\"icon\" alt=\"$str_thisname\" /> ";
			if ($hash_this["link"]) $str_result .= "<a href=\"$str_thislink\"> ";
			if ($hash_this["classtext"]) $str_result .= "<span class=\"".$hash_this["classtext"]."\">"; 
			if ($hash_this["text"]) $str_result .= $hash_this["text"];
			if ($hash_this["classtext"]) $str_result .= "</span> "; 
	      if ($hash_this["link"]) $str_result .= "</a> ";
			# /li
	      $str_result .= "</li> \n";
         }
		# /ol
      $str_result .= "</ol> \n";
		#
      return $str_result;	
      }
      
      
   function ah2Select ($ah_this, $str_id='', $str_name='data', $str_class='') // not support to optgroup
      {
		# select
      $str_result = "<select ";
		$str_result .= ($str_id) ? 'id="'.$str_id.'" ' : 'id="'.$str_name.'" ';
		$str_result .= "name=\"$str_name\" ";
      if ($str_class) $str_result .= 'class="'.$str_class.'" ';
      $str_result .= "> \n";
		# ah_this
      foreach ($ah_this as $hash_this) {
         # option
         $str_result .= '<option ';
			if (($hash_this["id"]) || ($hash_this["name"])) $str_result .= ($hash_this['id']) ? 'id="'.$hash_this["id"].'" ' : 'id="'.$hash_this["name"].'" ';
			if ($hash_this["name"]) $str_result .= 'name="'.$hash_this["name"].'" '; 
			if ($hash_this["class"]) $str_result .= 'class="'.$hash_this["class"].'" ';
			$str_result .= "value=\"".$hash_this["val"]."\" ";
			if ($hash_this["selected"]) $str_result .= 'selected="selected" ';
			if ($hash_this["disabled"]) $str_result .= 'disabled="disabled" '; 
			$str_result .= '>';
			# option contents
			if ($hash_this["classtext"]) $str_result .= "<span class=\"".$hash_this["classtext"]."\">"; 
			if ($hash_this["text"]) $str_result .= $hash_this["text"];
			if ($hash_this["classtext"]) $str_result .= "</span> ";  
         # /select
         $str_result .= "</option> \n";
         }
      # /select
      $str_result .= "</select> \n";
		#
      return $str_result;	
      }   
      
      
   function ah2RadioList ($ah_this, $str_id='', $str_name='', $str_class='') // not support to fieldset; list controlled by class list-inline list-nobullet
      {
		# ul
      $str_result = '<ul '; 
		if (($str_id) || ($str_name)) $str_result .= ($str_id) ? 'id="'.$str_id.'" ' : 'id="'.$str_name.'" ';
		if ($str_name) $str_result .= "name=\"$str_name\" ";
      if ($str_class) $str_result .= 'class="'.$str_class.'" ';
      $str_result .= "> \n"; 
		# ah_this
		$int_memoryid = -1;
		$str_memoryname = 'data';
      foreach ($ah_this as $hash_this) {
			# set memory
			if ($hash_this["name"]) {
				$int_memoryid = 0;
				$str_memoryname = $hash_this["name"]; 
			   }
			# li input radio
         $str_result .= '<li><input type="radio" ';
			$str_result .= ($hash_this["id"]) ? "id=\"".$hash_this["id"]."\" " : "id=\"".$str_memoryname.$int_memoryid++."\" ";
			$str_result .= "name=\"$str_memoryname\" ";
			if ($hash_this["class"]) $str_result .= 'class="'.$hash_this["class"].'" ';
			$str_result .= "value=\"".$hash_this["val"]."\" ";
			if ($hash_this["checked"]) $str_result .= 'checked="checked" ';
			if ($hash_this["disabled"]) $str_result .= 'disabled="disabled" '; 
			if ($hash_this["readonly"]) $str_result .= 'readonly="readonly" ';
			$str_result .= '/> ';
			# input text 
         if ($hash_this["icon"]) $str_result .= "<img src=\"$str_thisicon\" class=\"icon\" alt=\"$str_thisname\" /> ";
			if ($hash_this["link"]) $str_result .= "<a href=\"$str_thislink\"> ";
			if ($hash_this["classtext"]) $str_result .= "<span class=\"".$hash_this["classtext"]."\">"; 
			if ($hash_this["text"]) $str_result .= $hash_this["text"];
			if ($hash_this["classtext"]) $str_result .= "</span> "; 
	      if ($hash_this["link"]) $str_result .= "</a> " ;
         # /li
	      $str_result .= "</li> \n"; 
         }
		# /ul	
		$str_result .=  "</ul> \n";
		#
      return $str_result;	
      }   
       
      
   function ah2CheckboxList ($ah_this, $str_id='', $str_name='', $str_class='') // not support to fieldset; list controlled by class list-inline list-nobullet
      {
 		# ul
      $str_result = '<ul '; 
		if (($str_id) || ($str_name)) $str_result .= ($str_id) ? 'id="'.$str_id.'" ' : 'id="'.$str_name.'" ';
		if ($str_name) $str_result .= "name=\"$str_name\" ";
      if ($str_class) $str_result .= 'class="'.$str_class.'" ';
      $str_result .= "> \n";  
		# ah_this
      foreach ($ah_this as $hash_this) {
			# li input radio
         $str_result .= '<li><input type="checkbox" ';
			$str_result .= ($hash_this["id"]) ? "id=\"".$hash_this["id"]."\" " : "id=\"".$hash_this["name"]."\" ";
			$str_result .= "name=\"".$hash_this["name"]."\" ";
			if ($hash_this["class"]) $str_result .= 'class="'.$hash_this["class"].'" ';
			$str_result .= "value=\"".$hash_this["val"]."\" ";
			if ($hash_this["checked"]) $str_result .= 'checked="checked" ';
			if ($hash_this["disabled"]) $str_result .= 'disabled="disabled" '; 
			if ($hash_this["readonly"]) $str_result .= 'readonly="readonly" ';
			$str_result .= '/> ';
			# input text 
         if ($hash_this["icon"]) $str_result .= "<img src=\"$str_thisicon\" class=\"icon\" alt=\"$str_thisname\" /> ";
			if ($hash_this["link"]) $str_result .= "<a href=\"$str_thislink\"> ";
			if ($hash_this["classtext"]) $str_result .= "<span class=\"".$hash_this["classtext"]."\">"; 
			if ($hash_this["text"]) $str_result .= $hash_this["text"];
			if ($hash_this["classtext"]) $str_result .= "</span> "; 
	      if ($hash_this["link"]) $str_result .= "</a> " ;
         # /li
	      $str_result .= "</li> \n"; 
         }
      return $str_result;	
      }   
      
      
#
#  param (hash/struct of input values) -> html structure 
#      

   function param2Hidden ($strct_this)
      {
      $str_result = '';
      foreach ($strct_this as $str_thisname => $str_thisval) {  
			if (is_numeric($str_thisval))
			   { $str_result .= "\t<input type=\"hidden\" name=\"$str_thisname\" value=\"$str_thisval\" /> \n";  }	
			elseif (is_string($str_thisval)) {
			   $str_thisval = htmlspecialchars($str_thisval);
	         $str_result .= "\t<input type=\"hidden\" name=\"$str_thisname\" value=\"$str_thisval\" /> \n"; 
			   }		
			elseif (is_array($str_thisval)) {
				foreach ($str_thisval as $str_thissubval) {
			      $str_thissubval = htmlspecialchars($str_thissubval);
	            $str_result .= "\t<input type=\"hidden\" name=\"$str_thisname".'[]'."\" value=\"$str_thisval\" /> \n"; 
				   } 
			   }
         } 		
      return $str_result;	
      }   
      
		
   function param2Get ($strct_this)
      { 
      $str_result = '';	
      foreach ($strct_this as $str_thisname => $str_thisval) {  
			if (is_numeric($str_thisval))
			   { $str_result .= "&$str_thisname=$str_thisval"; }
			elseif (is_string($str_thisval)) {
			   $str_thisval = urlencode($str_thisval);
	         $str_result .= "&$str_thisname=$str_thisval";  
			   }
			elseif (is_array($str_thisval)) {
				foreach ($str_thisval as $str_thissubval) {
			      $str_thissubval = urlencode($str_thissubval);
	            $str_result .= "$str_thisname".'[]'."=$str_thissubval"; 					
				   } 
			   }
         } 
      return preg_replace('/^&/','',$str_result);	
      }   
    
       
?>