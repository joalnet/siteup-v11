<?php

# ============================================================
#    SiteUP!  (c)  J. Alejandro Ceballos Z.
#      Name:  debug.php - Build 1506
#   License:  MIT - https://tldrlegal.com/license/mit-license
# ============================================================
#

# Block direct access
   if (preg_match("/debug/",$_SERVER['PHP_SELF'])) die('= direct access disabled ='); 
   if ($bool_DEBUG) shout('p','debug'); 

   
#
# ========================================= [ FUNCTIONS ]
#

#
# Show status

  function Shout($char_class, $obj_this)
    { 
    #
    switch ($char_class)
      {
      case 'p': {
        print "<br><br><strong style=\"color:#F93;\">SECTION:</strong> $obj_this \n";
        break; } 
      case 'i': {
        print "<br><strong style=\"color:#9CF;\">INFO STATUS:</strong> $obj_this \n";
        break; }
      case 'e': {
        print "<br><strong style=\"color:#600;\">ERROR:</strong> $obj_this \n";
        break; } 
      case 'q': {
        print "<br><strong style=\"color:#33C;\">QUERY:</strong> $obj_this \n";
        break; }
      case 'r': {
        print "<br><strong style=\"color:#669;\">RESULT:</strong> $obj_this \n";
        break; }
      case 's': {
        print "<br><strong style=\"color:#06F;\">SYSTEM:</strong> $obj_this \n";
        break; }
      case '@': { 
        print "<br><strong style=\"color:#C90;\">ARRAY:</strong> <ol><li>".join ("</li>\n<li>",$obj_this)."</li></ol> \n";         
        break; }
      case '%': { 
        print "<br><strong style=\"color:#093;\">HASH:</strong> <ul> \n";
        foreach (array_keys($obj_this) as $str_thiskey) { print "  <li>$str_thiskey: $obj_this[$str_thiskey] </li> \n"; }
        print "</ul> ";
        break; }
      case '?': {
        print "<br><strong style=\"color:#90F;\">STRUCTURE:</strong> <ul> \n";
        print "<br><strong style=\"color:palegreen4;\"> ".gettype($obj_this)." </strong>".var_export($obj_this)." \n";
        break; }
      default: {
        print "<br><strong style=\"color:#699;\">-:</strong> $obj_this \n";
        break; }
      }   
    }


#
# Init debug

  function BeginDebug()
    {
    $i = time();
    print "<h1>".$_SERVER["SCRIPT_FILENAME"]." DEBUG $i - PHP version ".phpversion()."</h1> \n"
       . "<h2>Requests</h2><small>";
    foreach (array_keys($_REQUEST) as $str_thiskey)
      {
      print " $str_thiskey: $_REQUEST[$str_thiskey] ";
      if ($_COOKIE[$str_thiskey]) { print " <span style=\"color:darkpink\">[COOKIE]</span> "; }
      print "<br> \n";
      }   
    print "</small> \n<hr>\n"; 
    }

#
# End debug

  function EndDebug()
    { Shout('p',"<blockquote><h3><a href=\"".$_SERVER["SCRIPT_FILENAME"]."\"> NEXT </a></h3></blockquote>");  }

#
# Obscure string

  function Obscure($str_value)
    { 
    $int_quarter = floor(strlen($str_value)/4);
    $int_length = strlen($str_value);
    return substr($str_value,0,$int_quarter).str_repeat('-',$int_length-(2*$int_quarter)).substr($str_value,$int_length-$int_quarter); 
    }

?>