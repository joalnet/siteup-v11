<?php

# ============================================================
#   SiteUp! :   Modular Builder for Interactive Web Sites             
# ============================================================
#    Version:   10.0.1 
#       Name:   crypt.php
#       Date:   1506
#   Function:   Crypting and ID tools
# ------------------------------------------------------------
#       Uses:   -
# ------------------------------------------------------------
#  Copyright:   (c)  J. Alejandro Ceballos Z. 
#    License:   MIT - http://opensource.org/licenses/MIT
# ============================================================
#

# Block direct access
  if (preg_match("/crypt/",$_SERVER['PHP_SELF'])) {
    Header("Location: /403.shtml");
    die(); 
    } 
  if ($bool_DEBUG) { Shout('p','crypt'); }


#
# ========================================= [ LIBRARIES, CONSTANTS AND VARIABLES ]
#

#
# -------------------------------- [ Constants ]

# Encrypting data
   $arr_ENCODINGCHARS = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
   $int_ENCODINGCHARS = count($arr_ENCODINGCHARS);      


#
# ========================================= [ FUNCTIONS ]
#
#
# Generate new ID

   function newID ($int_SIZEID=32)
      { 
      #    
      $real_now = gettimeofday(true); 
      $arr_ip = preg_split('/\./',$_SERVER["HTTP_X_FORWARDED_FOR"] or $_SERVER["REMOTE_ADDR"] or $_SERVER["HTTP_HOST"]);  
      $arr_thisval = array(
	time()-900000000,
	(count($arr_ip) == 4) ? ((((($arr_ip[0] *255)+$arr_ip[1])*255)+$arr_ip[2])*255)+$arr_ip[3] : int(rand(7919)),
	int(rand(7919)) % $int_ENCODINGCHARS,
	int(((gettimeofday(true)-$real_now)*1000000) % 1000000)
         );
      for ($i=3; $i>=0; $i--) {
         $int_thisval = $arr_thisval[$i];
         while ($int_thisval) {
            $str_thisID .= $arr_ENCODINGCHARS[$int_thisval % $int_ENCODINGCHARS];
            $int_thisval = int($int_thisval/$int_ENCODINGCHARS);
            }
         }
      if (length($str_thisID)<$int_SIZEID) {
         $str_thisID .= '_';
         while (length($str_thisID)<$int_SIZEID)
            { $str_thisID .= $arr_ENCODINGCHARS[int(rand($int_ENCODINGCHARS))]; }
         }
      return $str_thisID; 
      }


#
# Encode Password as 28 caracters

   function encodePwd ($str_pwd,$str_sec)
      { 
      global $arr_ENCODINGCHARS, $int_ENCODINGCHARS;
      # change to 1 for more security, but not portable.
      # Change AFTER webmaster first login and while logged, before add any other admin or user to system; and later change admin password without logout
      # Change it in crypt.pl too
      $bool_USECRYPT = 0;
      #
      $str_pwd = preg_replace('/\s/','',$str_pwd);
      $str_encoded = ($bool_USECRYPT) ? crypt($str_pwd,substr($str_sec,0,2)) : strrev($str_pwd); 
      $str_encoded = preg_replace('/\W/','',$str_pwd); 
      $arr_data = preg_split('//',md5($str_pwd).substr(md5($str_encoded),0,10));
      for ($i=0, $str_encoded=''; $i<28; $i++) {
         $int_val = ($i%2) ? floor(hexdec($arr_data[floor($i/2)+28])/4) : hexdec($arr_data[floor($i/2)+28]) % 4;
         $int_val += hexdec($arr_data[$i])*4;
         if ($int_val < $int_ENCODINGCHARS)
            { $str_encoded .= $arr_ENCODINGCHARS[$int_val]; }
         else
            { $str_encoded .= ($int_val == $int_ENCODINGCHARS) ? '-' : '_'; }
         }
      return $str_encoded;
      }

     
#
# Generate session value

   function sessionValue()
      {
      $str_seed = encode_base64(md5_hex(time())); 
      return substr(preg_replace('\W','',$str_seed),0,12+int(rand(12)));
      }
 

#
# Encode data in a tabbed base 64   

   function encodeBase64TabbedData ($arr_data)
      { return base64_encode(join("\t",$arr_data)); }

#
# Decode a base 64 tabbed data

   function decodeBase64TabbedData ($str_data)
      { return explode('\t',base64_decode($str_data)); }

?>